package AbhikRai;

public class VerifyDeliveryPerson {

	public static boolean verifyfName(String fname) {
		String regex="[a-zA-Z]{3,15}";
		return fname.matches(regex);
	}
	public static boolean verifylName(String lname) {
		String regex="[a-zA-Z]{3,15}";
		return lname.matches(regex);
	}
	public static boolean verifyContactNumber(String phone) {
		// 9 digits phone number
		String regex = "[0-9]{9}";
		return phone.matches(regex);
	}
	public static boolean verifyEmailid(String email) {
		String regex = "(\\w+)([\\w+.-])*@(\\w+)([\\w+.-])*\\.\\w+";
		return email.matches(regex);
	}
	public static boolean verifyPassword(String password) {
		String regex="[0-9a-zA-Z]{6,18}";
		return password.matches(regex);
	}
	public static boolean verifyStatus(String status) {
		String regex="[0-2]{2}";
		return status.matches(regex);
	}
	public static boolean verifyArea(String area) {
		// 9 digits phone number
		String regex = "[0-25]{2}";
		return area.matches(regex);
	}
	
}
