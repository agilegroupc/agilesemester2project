package AbhikRai.GUI;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import AbhikRai.DB.Table.TBDeliveryPerson;
import Suriya.newsagent.DialogMessage;

@SuppressWarnings("serial")
public class QueryModel extends AbstractTableModel {
	Vector modelData; // will hold String[] objects
	int colCount;
	String[] headers = new String[0];
	Connection con;
	Statement stmt = null;
	String[] record;
	ResultSet rs = null;

	public QueryModel() {
		modelData = new Vector();
		initiate_db_conn();
	}// end constructor QueryTableModel

	public String getColumnName(int i) {
		return headers[i];
	}

	public int getColumnCount() {
		return colCount;
	}

	public int getRowCount() {
		return modelData.size();
	}

	public Object getValueAt(int row, int col) {
		return ((String[]) modelData.elementAt(row))[col];
	}

	public void initiate_db_conn() {
		try {
			// Load the JConnector Driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			// Specify the DB Name
			String url = "jdbc:mysql://localhost:3306/athlone_delivery_system?serverTimezone=UTC";
			// Connect to DB using DB URL, Username and password
			con = DriverManager.getConnection(url, "root", "mike");
			// Create a generic statement which is passed to the TestInternalFrame1
			stmt = con.createStatement();

		} catch (Exception e) {
			System.out.println("Error: Failed to connect to database\n" + e.getMessage());
		}
	}

	public void call(int in) {
		int inn = in;

		if (inn == 1) {
			showBills(stmt);
		} else if (inn == 2) {
			showFailedDeliveries(stmt);
		} else if (inn == 3) {
			showFailedOrders(stmt);
		} else if (inn == 4) {
			showDeliveryDocket(stmt);
		} else if (inn == 5) {
			showAllOrders(stmt);
		} else {
			DialogMessage.showInfoDialog("Please Select an option");
		}
	}

	public void showDeliveryDocket(Statement stmt1) {
		// modelData is the data stored by the table
		// when set query is called the data from the
		// DB is queried using �SELECT * FROM myInfo?
		// and the data from the result set is copied
		// into the modelData. Every time refreshFromDB is
		// called the DB is queried and a new
		// modelData is created

		modelData = new Vector();
		stmt = stmt1;
		try {
			// Execute the query and store the result set and its metadata
			//TBDeliveryPerson t = new TBDeliveryPerson();
			rs = stmt.executeQuery("SELECT * FROM DeliveryDocket");
//			rs = stmt.executeQuery(
//					"select delivery_id, orders.order_id, customers.customer_id, DeliveryPerson.dpId, delivery_date, deliverydocket_status from orders, customers, DeliveryPerson, DeliveryDocket "
//							+ "where DeliveryDocket.order_id=orders.order_id and DeliveryDocket.customer_id=customers.customer_id and DeliveryDocket.dpId=DeliveryPerson.dpId");
			ResultSetMetaData meta = rs.getMetaData();

			// to get the number of columns
			colCount = meta.getColumnCount();
			// Now must rebuild the headers array with the new column names
			headers = new String[colCount];

			for (int h = 0; h < colCount; h++) {
				headers[h] = meta.getColumnName(h + 1);
			} // end for loop

			// fill the cache with the records from the query, ie get all the rows

			while (rs.next()) {
				record = new String[colCount];
				for (int i = 0; i < colCount; i++) {
					record[i] = rs.getString(i + 1);
				} // end for loop
				modelData.addElement(record);
			} // end while loop
			fireTableChanged(null); // Tell the listeners a new table has arrived.
		} // end try clause
		catch (Exception e) {
			//System.out.println("Error with refreshFromDB Method\n" + e.toString());
		e.printStackTrace();
		} // end catch clause to query table
	}// end refreshFromDB method

	public void showBills(Statement stmt1) {
		modelData = new Vector();
		stmt = stmt1;
		try {
			// Execute the query and store the result set and its metadata
			rs = stmt.executeQuery("SELECT * FROM billing");
			ResultSetMetaData meta = rs.getMetaData();

			// to get the number of columns
			colCount = meta.getColumnCount();
			// Now must rebuild the headers array with the new column names
			headers = new String[colCount];

			for (int h = 0; h < colCount; h++) {
				headers[h] = meta.getColumnName(h + 1);
			} // end for loop

			// fill the cache with the records from the query, ie get all the rows

			while (rs.next()) {
				record = new String[colCount];
				for (int i = 0; i < colCount; i++) {
					record[i] = rs.getString(i + 1);
				} // end for loop
				modelData.addElement(record);
			} // end while loop
			fireTableChanged(null); // Tell the listeners a new table has arrived.
		} // end try clause
		catch (Exception e) {
			e.printStackTrace();
		} // end catch clause to query table
	}// end refreshFromDB method

	public void showFailedDeliveries(Statement stmt1) {
		modelData = new Vector();
		stmt = stmt1;
		try {
			// Execute the query and store the result set and its metadata
			rs = stmt.executeQuery("SELECT * FROM DeliveryDocket where deliverydocket_status = 0");
			// rs = stmt.executeQuery("select delivery_id, orders.order_id,
			// customers.customer_id, DeliveryPerson.dpId, delivery_date, ,
			// deliverydocket_status from orders, customers, DeliveryPerson, DeliveryDocket
			// where DeliveryDocket.order_id=orders.order_id and
			// DeliveryDocket.customer_id=customers.customer_id and
			// DeliveryDocket.dpId=DeliveryPerson.dpId and area= ");
			ResultSetMetaData meta = rs.getMetaData();

			// to get the number of columns
			colCount = meta.getColumnCount();
			// Now must rebuild the headers array with the new column names
			headers = new String[colCount];

			for (int h = 0; h < colCount; h++) {
				headers[h] = meta.getColumnName(h + 1);
			} // end for loop

			// fill the cache with the records from the query, ie get all the rows

			while (rs.next()) {
				record = new String[colCount];
				for (int i = 0; i < colCount; i++) {
					record[i] = rs.getString(i + 1);
				} // end for loop
				modelData.addElement(record);
			} // end while loop
			fireTableChanged(null); // Tell the listeners a new table has arrived.
		} // end try clause
		catch (Exception e) {
			e.printStackTrace();
		} // end catch clause to query table
	}// end refreshFromDB method

	public void showFailedOrders(Statement stmt1) {
		modelData = new Vector();
		stmt = stmt1;
		try {
			// Execute the query and store the result set and its metadata
			rs = stmt.executeQuery("SELECT * FROM Orders where order_status='0'");
			ResultSetMetaData meta = rs.getMetaData();

			// to get the number of columns
			colCount = meta.getColumnCount();
			// Now must rebuild the headers array with the new column names
			headers = new String[colCount];

			for (int h = 0; h < colCount; h++) {
				headers[h] = meta.getColumnName(h + 1);
			} // end for loop

			// fill the cache with the records from the query, ie get all the rows

			while (rs.next()) {
				record = new String[colCount];
				for (int i = 0; i < colCount; i++) {
					record[i] = rs.getString(i + 1);
				} // end for loop
				modelData.addElement(record);
			} // end while loop
			fireTableChanged(null); // Tell the listeners a new table has arrived.
		} // end try clause
		catch (Exception e) {
			e.printStackTrace();
		} // end catch clause to query table
	}// end refreshFromDB method

	public void showAllOrders(Statement stmt1) {
		modelData = new Vector();
		stmt = stmt1;
		try {
			// Execute the query and store the result set and its metadata
			rs = stmt.executeQuery("SELECT * FROM Orders");
			ResultSetMetaData meta = rs.getMetaData();

			// to get the number of columns
			colCount = meta.getColumnCount();
			// Now must rebuild the headers array with the new column names
			headers = new String[colCount];

			for (int h = 0; h < colCount; h++) {
				headers[h] = meta.getColumnName(h + 1);
			} // end for loop

			// fill the cache with the records from the query, ie get all the rows

			while (rs.next()) {
				record = new String[colCount];
				for (int i = 0; i < colCount; i++) {
					record[i] = rs.getString(i + 1);
				} // end for loop
				modelData.addElement(record);
			} // end while loop
			fireTableChanged(null); // Tell the listeners a new table has arrived.
		} // end try clause
		catch (Exception e) {
			e.printStackTrace();
		} // end catch clause to query table
	}// end refreshFromDB method

	public void outputToTable(String sql) {
		modelData = new Vector();
		try {
			// Execute the query and store the result set and its metadata
			rs = stmt.executeQuery(sql);
			ResultSetMetaData meta = rs.getMetaData();

			// to get the number of columns
			colCount = meta.getColumnCount();
			// Now must rebuild the headers array with the new column names
			headers = new String[colCount];

			for (int h = 0; h < colCount; h++) {
				headers[h] = meta.getColumnName(h + 1);
			} // end for loop

			// fill the cache with the records from the query, ie get all the rows

			while (rs.next()) {
				record = new String[colCount];
				for (int i = 0; i < colCount; i++) {
					record[i] = rs.getString(i + 1);
				} // end for loop
				modelData.addElement(record);
			} // end while loop
			fireTableChanged(null); // Tell the listeners a new table has arrived.
		} // end try clause
		catch (Exception e) {
			e.printStackTrace();
		} // end catch clause to query table
	}// end refreshFromDB method

	public int changeTable(String sql) {
		int num = -2;
		try {
			num = stmt.executeUpdate(sql);
			outputToTable("select * from orders;");
			return num;
		} // end try clause
		catch (Exception e) {
			e.printStackTrace();
		} // end catch clause to query table
		return num;
	}
}
