package AbhikRai.GUI;

import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import AbhikRai.DialogMessage;
import AbhikRai.VerifyDeliveryDocket;
import AbhikRai.DB.DAO.DeliveryPersonDAO;
import AbhikRai.DB.Table.TBDeliveryDocket;
import AbhikRai.DB.Table.TBDeliveryPerson;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;

public class DeliveryDocket {
	
	String cmd = null;

	// DB Connectivity Attributes
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;


	private JFrame frmDeliveryDocket;
	private JTextField DeliveryIDTF;
	private JTextField DeliveryPersonIDTF;
	private JTextField DeliveryDateTF;
	private JTextField DeliveryStatusTF;
	
	JLabel lblArea = new JLabel("Area,");
	JLabel arealbl = new JLabel("");
	
	private DeliveryPersonDAO dpDao=new DeliveryPersonDAO();
	QueryModel TableModel = new QueryModel();
	private JTable table = new JTable(TableModel);
	
	private int dpID=1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DeliveryDocket window = new DeliveryDocket();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	

	/**
	 * Create the application.
	 */
	public DeliveryDocket() {
		initialize();
		refresh();
		frmDeliveryDocket.setVisible(true);
		table.setModel(TableModel);
		TableModel.call(4);
	}
	
	public int getDpID() {
		return dpID;
	}

	public void setDpID(int dpID) {
		this.dpID = dpID;
	}


	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmDeliveryDocket = new JFrame();
		frmDeliveryDocket.setBounds(100, 100, 848, 585);
		frmDeliveryDocket.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmDeliveryDocket.getContentPane().setLayout(null);
		
		JLabel lbldd = new JLabel("DELIVERY DOCKET");
		lbldd.setBounds(270, 13, 265, 36);
		lbldd.setFont(new Font("Calibri", Font.BOLD, 29));
		frmDeliveryDocket.getContentPane().add(lbldd);
		
		JLabel DeliveryIDLabel = new JLabel("Delivery ID: ");
		DeliveryIDLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		DeliveryIDLabel.setBounds(52, 139, 186, 22);
		frmDeliveryDocket.getContentPane().add(DeliveryIDLabel);
		
		DeliveryIDTF = new JTextField();
		DeliveryIDTF.setBounds(270, 141, 189, 20);
		frmDeliveryDocket.getContentPane().add(DeliveryIDTF);
		DeliveryIDTF.setColumns(10);
		
		JLabel DeliveryPersonIDLabel = new JLabel("Delivery Person ID: ");
		DeliveryPersonIDLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		DeliveryPersonIDLabel.setBounds(52, 192, 170, 14);
		frmDeliveryDocket.getContentPane().add(DeliveryPersonIDLabel);
		
		DeliveryPersonIDTF = new JTextField();
		DeliveryPersonIDTF.setBounds(270, 190, 186, 20);
		frmDeliveryDocket.getContentPane().add(DeliveryPersonIDTF);
		DeliveryPersonIDTF.setColumns(10);
		
		JLabel DeliveryDateLabel = new JLabel("Delivery Date (YYYY-MM-DD): ");
		DeliveryDateLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		DeliveryDateLabel.setBounds(49, 232, 223, 14);
		frmDeliveryDocket.getContentPane().add(DeliveryDateLabel);
		
		DeliveryDateTF = new JTextField();
		DeliveryDateTF.setBounds(270, 230, 189, 20);
		frmDeliveryDocket.getContentPane().add(DeliveryDateTF);
		DeliveryDateTF.setColumns(10);
		
		JLabel DeliveryStatusLabel = new JLabel("Delivery Status: ");
		DeliveryStatusLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		DeliveryStatusLabel.setBounds(49, 271, 156, 14);
		frmDeliveryDocket.getContentPane().add(DeliveryStatusLabel);
		
		DeliveryStatusTF = new JTextField();
		DeliveryStatusTF.setBounds(270, 269, 189, 20);
		frmDeliveryDocket.getContentPane().add(DeliveryStatusTF);
		DeliveryStatusTF.setColumns(10);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 324, 804, 200);
		frmDeliveryDocket.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JButton btnInsert = new JButton("INSERT");
		btnInsert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(DeliveryIDTF.getText().trim().equals("") || DeliveryPersonIDTF.getText().trim().equals("") ||
						DeliveryDateTF.getText().trim().equals("") || DeliveryStatusTF.getText().trim().equals("")) {
					DialogMessage.showWarningDialog("Empty info!");
					return;
				}
				
				int dId = Integer.parseInt(DeliveryIDTF.getText());
				int dpId=Integer.parseInt(DeliveryPersonIDTF.getText());
				String dd=DeliveryDateTF.getText().trim();
				String ds= DeliveryStatusTF.getText().trim();
				
				TBDeliveryDocket ddoc=new TBDeliveryDocket();
				ddoc.setDeliveryId(dId);
				ddoc.setDeliveryPersonId(dpId);
				ddoc.setDeliveryDate(dd);
				ddoc.setDeliveryStatus(ds);
				int num = dpDao.insertDeliveryDocket(ddoc);
				if (num > 0) {
					DialogMessage.showInfoDialog("Transfered successfully!!!");
					TableModel.call(4);
					table.setModel(TableModel);
				}
			}
		});
		btnInsert.setBounds(501, 150, 97, 25);
		frmDeliveryDocket.getContentPane().add(btnInsert);
		
		lblArea.setFont(new Font("Calibri", Font.BOLD, 20));
		lblArea.setHorizontalAlignment(SwingConstants.CENTER);
		lblArea.setBounds(43, 61, 105, 46);
		frmDeliveryDocket.getContentPane().add(lblArea);
		
		arealbl.setFont(new Font("Calibri", Font.BOLD, 20));
		arealbl.setBounds(160, 61, 248, 46);
		frmDeliveryDocket.getContentPane().add(arealbl);
		
		JButton btnCopy = new JButton("COPY");
		btnCopy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int row = table.getSelectedRow();				
					DeliveryIDTF.setText(TableModel.getValueAt(row, 0).toString());
					DeliveryPersonIDTF.setText(TableModel.getValueAt(row, 1).toString());
					DeliveryDateTF.setText(TableModel.getValueAt(row, 5).toString());
					DeliveryStatusTF.setText(TableModel.getValueAt(row, 6).toString());
			}
		});
		btnCopy.setBounds(610, 150, 97, 25);
		frmDeliveryDocket.getContentPane().add(btnCopy);
		
		JButton btnClear = new JButton("CLEAR");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DeliveryIDTF.setText("");
				DeliveryPersonIDTF.setText("");
				DeliveryDateTF.setText("");
				DeliveryStatusTF.setText("");
			}
		});
		btnClear.setBounds(719, 150, 97, 25);
		frmDeliveryDocket.getContentPane().add(btnClear);
		
		JButton btnUpdateDeliveryStatus = new JButton("UPDATE DELIVERY STATUS");
		btnUpdateDeliveryStatus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
					String updateTemp ="UPDATE DeliveryDocket SET" +
					" dpId = "+DeliveryPersonIDTF.getText()+
					", delivery_date ='"+DeliveryDateTF.getText()+
					"', deliverydocket_status ="+DeliveryStatusTF.getText()+
					" where delivery_id = "+DeliveryIDTF.getText();

					int num=TableModel.changeTable(updateTemp);
					TableModel.call(4);
					if(num>0)
						DialogMessage.showInfoDialog("Updated Successfully!");
			}
		});
		btnUpdateDeliveryStatus.setBounds(501, 188, 223, 25);
		frmDeliveryDocket.getContentPane().add(btnUpdateDeliveryStatus);
		
		JButton btnShowFailedDeliveries = new JButton("SHOW FAILED DELIVERIES");
		btnShowFailedDeliveries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					TableModel.call(2);
			}
	});
		btnShowFailedDeliveries.setBounds(501, 228, 241, 25);
		frmDeliveryDocket.getContentPane().add(btnShowFailedDeliveries);
		
		JButton btnBack = new JButton("BACK");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new MainPageDP();
				frmDeliveryDocket.dispose();
			}
		});
		btnBack.setBounds(501, 267, 97, 25);
		frmDeliveryDocket.getContentPane().add(btnBack);
		
		refresh();
		
		frmDeliveryDocket.setLocationRelativeTo(null);

	}
	
	private void refresh() {
		try {
			//TBDeliveryPerson tbP = new TBDeliveryPerson();
			String area = dpDao.queryArea(dpID);
			arealbl.setText(area);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
}
