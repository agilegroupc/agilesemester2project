package AbhikRai.GUI;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import AbhikRai.DialogMessage;
import AbhikRai.VerifyDeliveryPerson;
import AbhikRai.DB.DAO.DeliveryPersonDAO;
import AbhikRai.DB.Table.TBDeliveryPerson;

public class RegisterDP {

	private JFrame frmDeliveryPersonRegister;
	private JTextField txtdpfName;
	private JTextField txtdplName;
	private JTextField txtContactNumber;
	private JTextField txtEmailId;
	private JPasswordField txtPassword;
	private JTextField txtdpStatus;
	private JTextField txtdpArea;
	private DeliveryPersonDAO dpDao=new DeliveryPersonDAO();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegisterDP window = new RegisterDP();
					window.frmDeliveryPersonRegister.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public RegisterDP() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmDeliveryPersonRegister = new JFrame();
		frmDeliveryPersonRegister.setBounds(100, 100, 572, 492);
		frmDeliveryPersonRegister.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmDeliveryPersonRegister.getContentPane().setLayout(null);
		
		JLabel lblRegister = new JLabel("Register As Delivery Personnel");
		lblRegister.setBounds(95, 25, 395, 36);
		lblRegister.setFont(new Font("Calibri", Font.BOLD, 29));
		frmDeliveryPersonRegister.getContentPane().add(lblRegister);
		
		JLabel lbldpfName = new JLabel("First Name: ");
		lbldpfName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lbldpfName.setBounds(49, 91, 186, 22);
		frmDeliveryPersonRegister.getContentPane().add(lbldpfName);
		
		txtdpfName = new JTextField();
		txtdpfName.setBounds(262, 95, 189, 20);
		frmDeliveryPersonRegister.getContentPane().add(txtdpfName);
		txtdpfName.setColumns(10);
		
		JLabel lbContactNumber = new JLabel("Contact Number: ");
		lbContactNumber.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lbContactNumber.setBounds(50, 183, 117, 14);
		frmDeliveryPersonRegister.getContentPane().add(lbContactNumber);
		
		txtContactNumber = new JTextField();
		txtContactNumber.setBounds(262, 183, 186, 20);
		frmDeliveryPersonRegister.getContentPane().add(txtContactNumber);
		txtContactNumber.setColumns(10);
		
		JLabel lblEmailID = new JLabel("Email Id: ");
		lblEmailID.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEmailID.setBounds(49, 220, 88, 14);
		frmDeliveryPersonRegister.getContentPane().add(lblEmailID);
		
		txtEmailId = new JTextField();
		txtEmailId.setBounds(262, 220, 189, 20);
		frmDeliveryPersonRegister.getContentPane().add(txtEmailId);
		txtEmailId.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password: ");
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPassword.setBounds(49, 256, 100, 14);
		frmDeliveryPersonRegister.getContentPane().add(lblPassword);
		
		txtPassword = new JPasswordField();
		txtPassword.setBounds(262, 256, 189, 20);
		frmDeliveryPersonRegister.getContentPane().add(txtPassword);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new LoginDP();
				frmDeliveryPersonRegister.dispose();
			}
		});
		btnBack.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnBack.setBounds(114, 398, 100, 28);
		frmDeliveryPersonRegister.getContentPane().add(btnBack);
		
		JLabel dplName = new JLabel("Last Name: ");
		dplName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		dplName.setBounds(49, 135, 186, 22);
		frmDeliveryPersonRegister.getContentPane().add(dplName);
		
		txtdplName = new JTextField();
		txtdplName.setColumns(10);
		txtdplName.setBounds(262, 139, 189, 20);
		frmDeliveryPersonRegister.getContentPane().add(txtdplName);
		
		JLabel dpstatus = new JLabel("Status: ");
		dpstatus.setFont(new Font("Tahoma", Font.PLAIN, 14));
		dpstatus.setBounds(49, 292, 100, 14);
		frmDeliveryPersonRegister.getContentPane().add(dpstatus);
		
		JLabel dpArea = new JLabel("Area: ");
		dpArea.setFont(new Font("Tahoma", Font.PLAIN, 14));
		dpArea.setBounds(49, 329, 100, 14);
		frmDeliveryPersonRegister.getContentPane().add(dpArea);
		
		txtdpStatus = new JTextField();
		txtdpStatus.setBounds(262, 292, 189, 20);
		frmDeliveryPersonRegister.getContentPane().add(txtdpStatus);
		
		txtdpArea = new JTextField();
		txtdpArea.setBounds(262, 329, 47, 20);
		frmDeliveryPersonRegister.getContentPane().add(txtdpArea);
		
		JButton btnRegister = new JButton("Register");
		btnRegister.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(txtdpfName.getText().trim().equals("") ||txtdplName.getText().trim().equals("") || txtContactNumber.getText().trim().equals("") ||
						txtEmailId.getText().trim().equals("") || txtPassword.getText().trim().equals("") ||  txtdpStatus.getText().trim().equals("") ||  txtdpArea.getText().trim().equals("")) {
					DialogMessage.showWarningDialog("Empty register info!");
					return;
				}
				String fn=txtdpfName.getText().trim();
				String ln=txtdplName.getText().trim();
				String ph=txtContactNumber.getText().trim();
				String mail=txtEmailId.getText().trim();
				String pass=txtPassword.getText();
				String status=txtdpStatus.getText();
				String area = txtdpArea.getText();
				
				if(!VerifyDeliveryPerson.verifyfName(fn)) {
					DialogMessage.showWarningDialog("Invalid first name format!");
					return;
				}
				if(!VerifyDeliveryPerson.verifylName(ln)) {
					DialogMessage.showWarningDialog("Invalid last name format!");
					return;
				}
				if(!VerifyDeliveryPerson.verifyContactNumber(ph)) {
					DialogMessage.showWarningDialog("Invalid phone format!");
					return;
				}
				if(!VerifyDeliveryPerson.verifyEmailid(mail)) {
					DialogMessage.showWarningDialog("Invalid email format!");
					return;
				}
				if(!VerifyDeliveryPerson.verifyPassword(pass)) {
					DialogMessage.showWarningDialog("Invalid password format!");
					return;
				}
				if(!VerifyDeliveryPerson.verifyStatus(status)) {
					DialogMessage.showWarningDialog("Invalid Status format!");
					return;
				}
				if(!VerifyDeliveryPerson.verifyArea(area)) {
					DialogMessage.showWarningDialog("Invalid Area format!");
					return;
				}
				TBDeliveryPerson dp=new TBDeliveryPerson();
				dp.setFirstName(fn);
				dp.setLastName(ln);
				dp.setPhone(ph);
				dp.setEmail(mail);
				dp.setPassword(pass);
				dp.setAvailable_status(status);
				dp.setArea(area);
				int num=dpDao.registerInsert(dp);
				if (num > 0) {
					DialogMessage.showInfoDialog("Register successfully!!!");
					new MainPageDP();
					frmDeliveryPersonRegister.dispose();
				}
			}
		});
		btnRegister.setBounds(307, 401, 111, 23);
		frmDeliveryPersonRegister.getContentPane().add(btnRegister);
		
		frmDeliveryPersonRegister.setLocationRelativeTo(null);
		frmDeliveryPersonRegister.setVisible(true);
	}
}
