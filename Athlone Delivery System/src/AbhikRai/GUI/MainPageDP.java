package AbhikRai.GUI;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import AbhikRai.DialogMessage;
import AbhikRai.VerifyDeliveryPerson;
import AbhikRai.DB.DAO.DeliveryPersonDAO;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JScrollPane;

public class MainPageDP{

	private JFrame frmDeliveryPersonMainPage;
	private int dpID=1;
	private DeliveryPersonDAO dpDao=new DeliveryPersonDAO();

	JLabel lblWellcome = new JLabel("Welcome,");
	JLabel dpNamelbl = new JLabel("");
	JLabel lblStatus = new JLabel("Available Status: ");
	JLabel statuslbl = new JLabel("");
	JTextField statusTF = new JTextField();

	JButton btnUpdateStatus = new JButton("Update Status");
 

	/**
	 * Create the application.
	 */
	public MainPageDP(){
		initialize();
	}
	
	public int getDpID() {
		return dpID;
	}

	public void setDpID(int dpID) {
		this.dpID = dpID;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		MainPageDP window = new MainPageDP();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		statusTF.setFont(new Font("Calibri", Font.PLAIN, 18));
		statusTF.setBounds(195, 143, 187, 38);
		statusTF.setColumns(10);
		frmDeliveryPersonMainPage = new JFrame();
		frmDeliveryPersonMainPage.setTitle("Delivery Personnel Main Page");
		frmDeliveryPersonMainPage.setBounds(100, 100, 513, 413);
		frmDeliveryPersonMainPage.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmDeliveryPersonMainPage.getContentPane().setLayout(null);
		lblWellcome.setFont(new Font("Calibri", Font.BOLD, 20));
		
		lblWellcome.setHorizontalAlignment(SwingConstants.CENTER);
		lblWellcome.setBounds(35, 13, 105, 46);
		frmDeliveryPersonMainPage.getContentPane().add(lblWellcome);
		dpNamelbl.setFont(new Font("Calibri", Font.BOLD, 20));
		
		dpNamelbl.setBounds(152, 13, 248, 46);
		frmDeliveryPersonMainPage.getContentPane().add(dpNamelbl);
		lblStatus.setFont(new Font("Calibri", Font.PLAIN, 18));
		
		lblStatus.setHorizontalAlignment(SwingConstants.CENTER);
		lblStatus.setBounds(35, 103, 132, 38);
		frmDeliveryPersonMainPage.getContentPane().add(lblStatus);
		btnUpdateStatus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(statusTF.getText().trim().equals("")) {
					DialogMessage.showErrorDialog("Empty status!");
					return;
				}
				if(!VerifyDeliveryPerson.verifyStatus(statusTF.getText())) {
					DialogMessage.showErrorDialog("Invalid Status!");
					return;
				}
				int num=0;
				try {
					num=dpDao.updateStatus(statusTF.getText(), dpID);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				if(num>0) {
					refresh();
					DialogMessage.showInfoDialog("Update successful!");
					return;
				}
				DialogMessage.showErrorDialog("Update failed!");
			}
		});
		
		btnUpdateStatus.setBounds(195, 204, 132, 38);
		frmDeliveryPersonMainPage.getContentPane().add(btnUpdateStatus);
		statuslbl.setFont(new Font("Calibri", Font.PLAIN, 18));
		
		statuslbl.setHorizontalAlignment(SwingConstants.LEFT);
		statuslbl.setBounds(195, 92, 105, 38);
		frmDeliveryPersonMainPage.getContentPane().add(statuslbl);
		
		frmDeliveryPersonMainPage.getContentPane().add(statusTF);
		
		JButton btnViewDeliveryDockets = new JButton("Delivery Dockets");
		btnViewDeliveryDockets.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new DeliveryDocket();
				frmDeliveryPersonMainPage.dispose();
			}
		});
		btnViewDeliveryDockets.setFont(new Font("Calibri", Font.PLAIN, 18));
		btnViewDeliveryDockets.setBounds(35, 291, 227, 38);
		frmDeliveryPersonMainPage.getContentPane().add(btnViewDeliveryDockets);
		
		JLabel lblNewStatus = new JLabel("New Status:");
		lblNewStatus.setFont(new Font("Calibri", Font.PLAIN, 18));
		lblNewStatus.setBounds(35, 143, 133, 36);
		frmDeliveryPersonMainPage.getContentPane().add(lblNewStatus);
		
		JButton btnNewButton = new JButton("Back");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new LoginDP();
				frmDeliveryPersonMainPage.dispose();
			}
		});
		btnNewButton.setBounds(378, 295, 97, 31);
		frmDeliveryPersonMainPage.getContentPane().add(btnNewButton);
		
		JLabel lblArea = new JLabel("Area: ");
		lblArea.setFont(new Font("Calibri", Font.PLAIN, 18));
		lblArea.setBounds(45, 72, 56, 16);
		frmDeliveryPersonMainPage.getContentPane().add(lblArea);

		refresh();
		
		frmDeliveryPersonMainPage.setVisible(true);
	}
	
	private void refresh() {
		try {
			String []name=dpDao.queryName(dpID);
			String addr=dpDao.queryStatus(dpID);
			statuslbl.setText(addr);
			dpNamelbl.setText(name[0]+" "+name[1]);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
}
