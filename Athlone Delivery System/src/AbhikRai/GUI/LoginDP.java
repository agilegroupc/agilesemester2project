package AbhikRai.GUI;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import AbhikRai.DialogMessage;
import AbhikRai.VerifyDeliveryPerson;
import AbhikRai.DB.DAO.DeliveryPersonDAO;
import StartupGUI.TempSartup;

import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class LoginDP {

	private JFrame frmDd;
	private JTextField EmailTF;
	private JPasswordField passwordField;
	private DeliveryPersonDAO deDao=new DeliveryPersonDAO();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		LoginDP window = new LoginDP();
	}

	/**
	 * Create the application.
	 */
	public LoginDP() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmDd = new JFrame();
		frmDd.setTitle("Delivery Personnel Log in Page");
		frmDd.setResizable(false);
		frmDd.setBounds(100, 100, 577, 510);
		frmDd.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmDd.getContentPane().setLayout(null);
		
		JLabel emailLabel = new JLabel("Email: ");
		emailLabel.setFont(new Font("Calibri", Font.PLAIN, 18));
		emailLabel.setBounds(120, 99, 97, 32);
		frmDd.getContentPane().add(emailLabel);
		
		JLabel PasswordLabel = new JLabel("Password: ");
		PasswordLabel.setFont(new Font("Calibri", Font.PLAIN, 18));
		PasswordLabel.setBounds(120, 180, 108, 32);
		frmDd.getContentPane().add(PasswordLabel);
		
		EmailTF = new JTextField();
		EmailTF.setFont(new Font("Calibri", Font.PLAIN, 18));
		EmailTF.setBounds(293, 98, 176, 35);
		frmDd.getContentPane().add(EmailTF);
		EmailTF.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setFont(new Font("Calibri", Font.PLAIN, 18));
		passwordField.setBounds(293, 180, 176, 32);
		frmDd.getContentPane().add(passwordField);
		
		JButton Loginbtn = new JButton("Log in");
		Loginbtn.setFont(new Font("Calibri", Font.PLAIN, 18));
		Loginbtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String email=EmailTF.getText();
				String pass=passwordField.getText();
				if(email.trim().equals("")||pass.trim().equals("")) {
					DialogMessage.showWarningDialog("Empty log in info!!!");
					return;
				}
				if(!VerifyDeliveryPerson.verifyEmailid(email)) {
					DialogMessage.showWarningDialog("Invalid email format!!!");
					return;
				}
				if(!VerifyDeliveryPerson.verifyPassword(pass)) {
					DialogMessage.showWarningDialog("Invalid password format!!!");
					return;
				}
				if(pass.equals(deDao.queryPassword(email))) {
					new MainPageDP();
					frmDd.dispose();
				}else if(deDao.queryPassword(email)!=null){
					DialogMessage.showWarningDialog("Wrong password!!!");
					return;
				}else {
					DialogMessage.showWarningDialog("Acccount does not exist!!!");
					return;
				}
			}
		});
		Loginbtn.setBounds(106, 285, 122, 35);
		frmDd.getContentPane().add(Loginbtn);
		
		JButton Registerbtn = new JButton("Register");
		Registerbtn.setFont(new Font("Calibri", Font.PLAIN, 18));
		Registerbtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new RegisterDP();
				frmDd.dispose();
			}
		});
		Registerbtn.setBounds(322, 285, 122, 35);
		frmDd.getContentPane().add(Registerbtn);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TempSartup objCall = new TempSartup();
				frmDd.dispose();
			}
		});
		btnBack.setBounds(226, 364, 108, 44);
		frmDd.getContentPane().add(btnBack);
		
		//center window
		frmDd.setLocationRelativeTo(null); 
		frmDd.setVisible(true);
	}
}
