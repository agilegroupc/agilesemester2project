package AbhikRai;

public class VerifyDeliveryDocket {
	
	public static boolean verifyFname(String fname) {
		String regex="[a-zA-Z]{3,15}";
		return fname.matches(regex);
	}
	public static boolean verifyLname(String lname) {
		String regex="[a-zA-Z]{3,15}";
		return lname.matches(regex);
	}
	public static boolean verifyStatus(String status) {
		String regex="[0-2]{2}";
		return status.matches(regex);
	}
	public static boolean verifyArea(String area) {
		String regex = "[0-25]{2}";
		return area.matches(regex);
	}
}
