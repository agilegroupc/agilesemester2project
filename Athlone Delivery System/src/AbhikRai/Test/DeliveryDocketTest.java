package AbhikRai.Test;

import org.junit.Test;

import AbhikRai.VerifyDeliveryDocket;
import junit.framework.TestCase;

//Test class to test inputs of Delivery Docket
public class DeliveryDocketTest extends TestCase {

	public void setUp() throws Exception {
		super.setUp();
	}
	
	//Test #1: To verify valid first name  
	//Inputs: Abhik
	//Output: Test pass
	@Test
	public void testVerifyfName() {
		assertEquals(true, VerifyDeliveryDocket.verifyFname("Abhik"));
	}
	
	//Test #2: To verify valid last name  
	//Inputs: Rai
	//Output: Test pass
	@Test
	public void testVerifylName() {
		assertEquals(true, VerifyDeliveryDocket.verifyLname("Rai"));
	}
	
	//Test #3: To verify valid delivery docket status  
	//Inputs: 01
	//Output: Test pass
	@Test
	public void testVerifyStatus() {
		assertEquals(true, VerifyDeliveryDocket.verifyStatus("01"));
	}
	
	//Test #4: To verify valid area 
	//Inputs: 12
	//Output: Test pass
	@Test
	public void testVerifyArea() {
		assertEquals(true, VerifyDeliveryDocket.verifyArea("12"));
	}

}

