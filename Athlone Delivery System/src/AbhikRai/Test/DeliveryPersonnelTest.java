package AbhikRai.Test;

import org.junit.Test;

import AbhikRai.VerifyDeliveryPerson;
import junit.framework.TestCase;

public class DeliveryPersonnelTest extends TestCase {

	public void setUp() throws Exception {
		super.setUp();
	}
	
	@Test
	public void testVerifyfName() {
		assertEquals(true, VerifyDeliveryPerson.verifyfName("Abhik"));
	}

	public void testVerifylName() {
		assertEquals(true, VerifyDeliveryPerson.verifyfName("Rai"));
	}
	@Test
	public void testVerifyContactNumber() {
		assertEquals(true, VerifyDeliveryPerson.verifyContactNumber("894206817"));
	}
	@Test
	public void testVerifyEmailid() {
		assertEquals(true, VerifyDeliveryPerson.verifyEmailid("abhik@gmail.com"));
	}
	@Test
	public void testVerifyPassword() {
		assertEquals(true, VerifyDeliveryPerson.verifyPassword("abhik123"));
	}
	
	public void testVerifyStatus() {
		assertEquals(true, VerifyDeliveryPerson.verifyStatus("01"));
	}
	public void testVerifyArea() {
		assertEquals(true, VerifyDeliveryPerson.verifyArea("12"));
	}

}
