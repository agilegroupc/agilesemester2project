package AbhikRai.Test;

import org.junit.Test;

import AbhikRai.VerifyBillingDetails;
import junit.framework.TestCase;

public class BillingTest extends TestCase {

	public void setUp() throws Exception {
		super.setUp();
	}

	//Test #1: To verify valid billing status  
	//Inputs: 01
	//Output: Test pass
	@Test
	public void testVerifyBillingStatus() {
		assertEquals(true, VerifyBillingDetails.verifyBillingStatus("01"));
	}
		
	//Test #2: To verify valid amount 
	//Inputs: 143.00
	//Output: Test pass
	@Test
	public void testVerifyAmount() {
		assertEquals(true, VerifyBillingDetails.verifyAmount("143.00"));
	}

}