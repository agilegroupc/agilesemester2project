package AbhikRai.Test;

import static org.junit.Assert.*;

import org.junit.Test;

import AbhikRai.DB.DAO.DeliveryPersonDAO;

public class testQueryPassword {

	@Test
	public void test() {
		DeliveryPersonDAO dpDao=new DeliveryPersonDAO();
		dpDao.setConnection("com.mysql.cj.jdbc.Driver",
				"jdbc:mysql://localhost:3306/athlone_delivery_system?serverTimezone=UTC", 
				"root", "mike");
		assertEquals("abc123", dpDao.queryPassword("mike@gmail.com"));
	}

}
