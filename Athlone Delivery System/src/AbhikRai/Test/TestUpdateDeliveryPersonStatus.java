package AbhikRai.Test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import AbhikRai.DB.DAO.DeliveryPersonDAO;

public class TestUpdateDeliveryPersonStatus {
	@Test
	public void test() throws Exception {
		DeliveryPersonDAO dpDao=new DeliveryPersonDAO();
		dpDao.setConnection("com.mysql.cj.jdbc.Driver",
				"jdbc:mysql://localhost:3306/athlone_delivery_system?serverTimezone=UTC", 
				"root", "admin");
			assertEquals(1, dpDao.updateStatus("1", 1));
	}
}
