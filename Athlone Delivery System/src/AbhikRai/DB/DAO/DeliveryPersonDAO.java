package AbhikRai.DB.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import AbhikRai.DB.DButils;
import AbhikRai.DB.Table.TBDeliveryDocket;
import AbhikRai.DB.Table.TBDeliveryPerson;
import AbhikRai.DB.Table.TBbill;

public class DeliveryPersonDAO {

	private static DButils dbc;
	private static Connection conn=null ;
	private static PreparedStatement st=null;
	private static ResultSet rs=null;
	
	public DeliveryPersonDAO() {
		try {
			dbc=DButils.getDBConnection();
			conn = dbc.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void setConnection(String driver,String url,String username,String password) {
		try {
			dbc.setConnection(driver, url, username, password);
			conn=dbc.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
	}
	
	public int registerInsert(TBDeliveryPerson dp) {
		int num=-2;
		try {
			String sql = "insert into deliveryperson(fName, lName, contact, email, password, deliveryperson_status, area) values(?,?,?,?,?,?,?)";
			st = conn.prepareStatement(sql);
			// index starts from 1
			st.setString(1, dp.getFirstName());
			st.setString(2, dp.getLastName());
			st.setString(3, dp.getPhone());
			st.setString(4, dp.getEmail());
			st.setString(5, dp.getPassword());
			st.setString(6, dp.getAvailable_status());
			st.setString(7, dp.getArea());
			// execute and return number of rows that take effect
			num = st.executeUpdate();
			return num;
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return num;
	}


	public String queryPassword(String email) {
		String status=null;
		String sql = "select password from deliveryperson where email=?";
		try {
			st = conn.prepareStatement(sql);
			st.setString(1, email);
			rs=st.executeQuery();
			while(rs.next()) {
				status=rs.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}
	
	public int updateStatus(String status, int dpId) throws Exception {
		int num=-1;
		String sql = "update deliveryperson set deliveryperson_status=? where dpId=?";
		try {
			st = conn.prepareStatement(sql);
			st.setString(1, status);
			st.setInt(2, dpId);
			num = st.executeUpdate();
//			if (num > 0) {
//				DialogMessage.showInfoDialog("Update successfully!!!");
//			}
			return num;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}
	
	public int queryID(String email) {
		int id=-1;
		String sql = "select dpId from deliveryperson where email=?";
		try {
			st = conn.prepareStatement(sql);
			st.setString(1, email);
			rs=st.executeQuery();
			while(rs.next()) {
				id=rs.getInt(1);
			}
		}catch (Exception e) {
				e.printStackTrace();
			}
		return id;
	}
	
	public String queryStatus(int dpId) {
		String status=null;
		String sql = "select deliveryperson_status from deliveryperson where dpId=?";
		try {
			st = conn.prepareStatement(sql);
			st.setInt(1, dpId);
			rs=st.executeQuery();
			while(rs.next()) {
				status=rs.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public String[] queryName(int dpId) {
		String []name=new String[2];
		String sql = "select fName,lName from deliveryperson where dpId=?";
		try {
			st = conn.prepareStatement(sql);
			st.setInt(1, dpId);
			rs=st.executeQuery();
			while(rs.next()) {
				name[0]=rs.getString(1);
				name[1]=rs.getString(2);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return name;
	}
	
	public int insertDeliveryDocket(TBDeliveryDocket dd) {
		int num=-2;
		try {
			String sql = "insert into DeliveryDocket(delivery_id, dpId, delivery_date, deliverydocket_status) values(?,?,?,?)";
			st = conn.prepareStatement(sql);
			// index starts from 1
			st.setInt(1, dd.getDeliveryId());
			st.setInt(2, dd.getDeliveryPersonId());
			st.setString(3, dd.getDeliveryDate());
			st.setString(4, dd.getDeliveryStatus());
			// execute and return number of rows that take effect
			num = st.executeUpdate();
			return num;
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return num;
	}
	
	public String queryArea(int dpId) {
		String area = null;
		String sql = "select area from deliveryperson where dpId=?";
		try {
			st = conn.prepareStatement(sql);
			st.setInt(1, dpId);
			rs=st.executeQuery();
			while(rs.next()) {
				area=rs.getString(1);
				TBDeliveryPerson tbP = new TBDeliveryPerson();
				tbP.setArea(area);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return area;
	}
	
	public int insertBill(TBbill bill) {
		int num=-2;
		try {
			String sql = "insert into billing(customer_id, order_id, billing_status, total_amount, overdue) values(?,?,?,?,?)";
			st = conn.prepareStatement(sql);
			// index starts from 1
			st.setInt(1, bill.getCustomerId());
			st.setInt(2, bill.getOrderId());
			st.setInt(3, bill.getBillstatus());
			st.setDouble(4, bill.getAmount());
			st.setInt(5, bill.getOverdue());
			// execute and return number of rows that take effect
			num = st.executeUpdate();
			return num;
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return num;
	}
	
	
}
