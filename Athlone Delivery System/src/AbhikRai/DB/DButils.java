package AbhikRai.DB;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class DButils {
	private static String driver = null;
	private static String url = null;
	private static String username = null;
	private static String password = null;
	private Connection conn;
	private PreparedStatement ps;
	private ResultSet rs;

	/*
	 * singleton pattern 
	 */
	private static class DBConnectionHolder{
		private static DButils dbc=new DButils();
	}
	
	/*
	 * lazy loading 
	 * DBConnection object don't initialize before invoking this method
	 */
	public static DButils getDBConnection() throws Exception {
		return DBConnectionHolder.dbc;
	}

	private DButils() {
		try {
			FileInputStream fin = new FileInputStream("./src/AbhikRai/DB/db.properties");
			Properties prop = new Properties();
			prop.load(fin);

			driver = prop.getProperty("driver");
			url = prop.getProperty("url");
			username = prop.getProperty("username");
			password = prop.getProperty("password");
			Class.forName(driver);
			conn=DriverManager.getConnection(url, username, password);
		} catch (Exception e) {
			throw new ExceptionInInitializerError(e);
		}
	}
	
	public void setConnection(String driver,String url,String username,String password) throws Exception{
		Class.forName(driver);
		conn=DriverManager.getConnection(url, username, password);
	}
	
	/**
	 * @Method: getConnection
	 * @return Connection
	 * @throws SQLException
	 */
	public Connection getConnection() throws Exception {
		return conn;
	}

	/**
	 * @Method: release
	 * @Description: release resources including Connection, Statement, ResultSet
	 *               objects
	 */
	public void release(Connection conn, Statement st, ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			rs = null;
		}
		if (st != null) {
			try {
				st.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (conn != null) {
			try {
				conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
}
