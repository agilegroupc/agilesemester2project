package AbhikRai.DB.Table;

public class TBbill {
	private int custid;
	private int orderid;
	private int billstatus; 
	private int amount;
	private int overdue;
	
	
	public int getCustomerId() {
		return custid;
	}
	public void setCustomerId(int custid) {
		this.custid = custid;
	}
	
	public int getOrderId() {
		return orderid;
	}
	public void setOrderId(int orderid) {
		this.orderid = orderid;
	}
	
	public int getBillstatus() {
		return billstatus;
	}
	public void setBillstatus(int billstatus) {
		this.billstatus = billstatus;
	}
	
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	public int getOverdue() {
		return overdue;
	}
	public void setOverdue(int overdue) {
		this.overdue = overdue;
	}
	
}
