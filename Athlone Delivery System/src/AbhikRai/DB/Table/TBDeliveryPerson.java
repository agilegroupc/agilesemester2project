package AbhikRai.DB.Table;

public class TBDeliveryPerson {
	private int dpId;
	private String firstName;
	private String lastName;
	private String phone;
	private String email; 
	private String password;
	private String available_status;
	private String area;
	
	public int getDeliverypersonId() {
		return dpId;
	}
	public void setDeliveryPersonId(int dpId) {
		this.dpId = dpId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAvailable_status() {
		return available_status;
	}
	public void setAvailable_status(String available_status) {
		this.available_status = available_status;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	
}
