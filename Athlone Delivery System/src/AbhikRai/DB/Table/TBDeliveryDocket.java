package AbhikRai.DB.Table;

public class TBDeliveryDocket {
	private int dId;
	private int dpId;
	private String dd;
	private String ds;
	
	public int getDeliveryId() {
		return dId;
	}
	public void setDeliveryId(int dId) {
		this.dId = dId;
	}
	
	
	public int getDeliveryPersonId() {
		return dpId;
	}
	public void setDeliveryPersonId(int dpId) {
		this.dpId = dpId;
	}
	
	
	public String getDeliveryDate() {
		return dd;
	}
	public void setDeliveryDate(String dd) {
		this.dd = dd;
	}
	
	
	public String getDeliveryStatus() {
		return ds;
	}
	public void setDeliveryStatus(String ds) {
		this.ds = ds;
	}
	
	
}
