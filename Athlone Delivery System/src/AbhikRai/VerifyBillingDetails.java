package AbhikRai;

public class VerifyBillingDetails {

public static boolean verifyBillingStatus(String billstatus) {
		String regex="[0-2]{2}";
		return billstatus.matches(regex);
	}
	public static boolean verifyAmount(String amt) {
		String regex = "\\d{1,3}\\.\\d{1,2}";
		return amt.matches(regex);
	}
}