package WangPengXin.GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.FlowLayout;
import javax.swing.JTextField;

import WangPengXin.DialogMessage;
import WangPengXin.VerifyUtils;
import WangPengXin.DB.DAO.CustomerDAO;
import WangPengXin.DB.Table.TBCustomer;

import javax.swing.JLabel;
import java.awt.Color;
import java.awt.SystemColor;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class CustomerRegister {

	private JFrame frmCustomerRegister;
	private JTextField firstName;
	private JTextField lastName;
	private JTextField age;
	private JTextField gender;
	private JTextField address;
	private JTextField phone;
	private JTextField email;
	private JLabel lblLastName;
	private JLabel lblAge;
	private JLabel lblGender;
	private JLabel lblAddress;
	private JLabel lblPhone;
	private JLabel lblEmail;
	private JLabel lblPassword;
	private JPasswordField password;
	JButton btnNewButton = new JButton("Register");
	JButton btnNewButton_1 = new JButton("Back");
	private CustomerDAO cusDao;

	/**
	 * Launch the application.
	 * 
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		CustomerRegister window = new CustomerRegister();
	}

	/**
	 * Create the application.
	 * 
	 * @throws Exception
	 */
	public CustomerRegister() throws Exception {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * 
	 * @throws Exception
	 */
	private void initialize() throws Exception {
		cusDao = new CustomerDAO();
		frmCustomerRegister = new JFrame();
		frmCustomerRegister.setResizable(false);
		frmCustomerRegister.setTitle("Customer Register");
		frmCustomerRegister.setBounds(100, 100, 605, 626);
		frmCustomerRegister.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCustomerRegister.getContentPane().setLayout(null);

		firstName = new JTextField();
		firstName.setBounds(284, 28, 170, 29);
		frmCustomerRegister.getContentPane().add(firstName);
		firstName.setColumns(10);

		lastName = new JTextField();
		lastName.setColumns(10);
		lastName.setBounds(284, 70, 170, 29);
		frmCustomerRegister.getContentPane().add(lastName);

		age = new JTextField();
		age.setColumns(10);
		age.setBounds(284, 112, 170, 29);
		frmCustomerRegister.getContentPane().add(age);

		gender = new JTextField();
		gender.setColumns(10);
		gender.setBounds(284, 154, 170, 29);
		frmCustomerRegister.getContentPane().add(gender);

		address = new JTextField();
		address.setColumns(10);
		address.setBounds(284, 196, 170, 29);
		frmCustomerRegister.getContentPane().add(address);

		phone = new JTextField();
		phone.setColumns(10);
		phone.setBounds(284, 238, 170, 29);
		frmCustomerRegister.getContentPane().add(phone);

		email = new JTextField();
		email.setColumns(10);
		email.setBounds(284, 280, 170, 29);
		frmCustomerRegister.getContentPane().add(email);

		password = new JPasswordField();
		password.setBounds(284, 324, 170, 24);
		frmCustomerRegister.getContentPane().add(password);

		JLabel lblNewLabel = new JLabel("First Name: ");
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 15));
		lblNewLabel.setBackground(Color.WHITE);
		lblNewLabel.setForeground(SystemColor.desktop);
		lblNewLabel.setBounds(95, 28, 126, 29);
		frmCustomerRegister.getContentPane().add(lblNewLabel);

		lblLastName = new JLabel("Last Name:");
		lblLastName.setFont(new Font("Arial", Font.BOLD, 15));
		lblLastName.setEnabled(true);
		lblLastName.setBounds(95, 70, 126, 29);
		frmCustomerRegister.getContentPane().add(lblLastName);

		lblAge = new JLabel("Age: ");
		lblAge.setFont(new Font("Arial", Font.BOLD, 15));
		lblAge.setBounds(95, 112, 126, 29);
		frmCustomerRegister.getContentPane().add(lblAge);

		lblGender = new JLabel("Gender: ");
		lblGender.setFont(new Font("Arial", Font.BOLD, 15));
		lblGender.setBounds(95, 154, 126, 29);
		frmCustomerRegister.getContentPane().add(lblGender);

		lblAddress = new JLabel("Address: ");
		lblAddress.setFont(new Font("Arial", Font.BOLD, 15));
		lblAddress.setBounds(95, 196, 126, 29);
		frmCustomerRegister.getContentPane().add(lblAddress);

		lblPhone = new JLabel("Phone:");
		lblPhone.setFont(new Font("Arial", Font.BOLD, 15));
		lblPhone.setBounds(95, 238, 126, 29);
		frmCustomerRegister.getContentPane().add(lblPhone);

		lblEmail = new JLabel("Email: ");
		lblEmail.setFont(new Font("Arial", Font.BOLD, 15));
		lblEmail.setBounds(95, 280, 126, 29);
		frmCustomerRegister.getContentPane().add(lblEmail);

		lblPassword = new JLabel("Password: ");
		lblPassword.setFont(new Font("Arial", Font.BOLD, 15));
		lblPassword.setBounds(95, 322, 126, 29);
		frmCustomerRegister.getContentPane().add(lblPassword);

		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if (firstName.getText().trim().equals("") || lastName.getText().trim().equals("")
							|| age.getText().trim().equals("") || gender.getText().trim().equals("")
							|| address.getText().trim().equals("") || phone.getText().trim().equals("")
							|| email.getText().trim().equals("") || password.getText().trim().equals("")) {
						DialogMessage.showWarningDialog("Empty register info!");
						return;
					}
					String fn = firstName.getText().trim();
					String ln = lastName.getText().trim();
					int ages = Integer.parseInt(age.getText().trim());
					String gen = gender.getText().trim();
					String addr = address.getText();
					String ph = phone.getText().trim();
					String mail = email.getText().trim();
					String pass = password.getText();

					if (!VerifyUtils.verifyName(fn)) {
						DialogMessage.showWarningDialog("Invalid first name format!");
						return;
					}
					if (!VerifyUtils.verifyName(ln)) {
						DialogMessage.showWarningDialog("Invalid last name format!");
						return;
					}
					if (!VerifyUtils.verifyAge(ages)) {
						DialogMessage.showWarningDialog("Invalid age format!");
						return;
					}
					if (!VerifyUtils.verifyGender(gen)) {
						DialogMessage.showWarningDialog("Invalid gender format!");
						return;
					}
					if (!VerifyUtils.verifyAddress(addr)) {
						DialogMessage.showWarningDialog("Invalid address format!");
						return;
					}
					if (!VerifyUtils.verifyPhone(ph)) {
						DialogMessage.showWarningDialog("Invalid phone format!");
						return;
					}
					if (!VerifyUtils.verifyEmail(mail)) {
						DialogMessage.showWarningDialog("Invalid email format!");
						return;
					}
					if (!VerifyUtils.verifyPasswordFormat(pass)) {
						DialogMessage.showWarningDialog("Invalid password format!");
						return;
					}
					TBCustomer customer = new TBCustomer();
					customer.setFirstName(fn);
					customer.setLastName(ln);
					customer.setAge(ages);
					customer.setGender(gen);
					customer.setAddress(addr);
					customer.setPhone(ph);
					customer.setEmail(mail);
					customer.setPassword(pass);
					int num = cusDao.customerInsert(customer);
					if (num > 0) {
						DialogMessage.showInfoDialog("Register successfully!!!");
						new CustomerLogin();
						frmCustomerRegister.dispose();
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(94, 394, 113, 27);
		frmCustomerRegister.getContentPane().add(btnNewButton);

		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					new CustomerLogin();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				frmCustomerRegister.dispose();
			}
		});
		btnNewButton_1.setBounds(293, 394, 113, 27);
		frmCustomerRegister.getContentPane().add(btnNewButton_1);

		// center window
		frmCustomerRegister.setLocationRelativeTo(null);
		frmCustomerRegister.setVisible(true);
	}
}
