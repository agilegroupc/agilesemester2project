package WangPengXin.GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import WangPengXin.DialogMessage;
import WangPengXin.VerifyUtils;
import WangPengXin.DB.DAO.OrderDAO;
import WangPengXin.DB.Table.TBOrders;

import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class PaymentPage {
	private int custID=0;
	private int itemID=0;
	private int amount=0;
	private String orderType="";
	private OrderDAO orderDao;
	private JFrame frmPaymentPage;
	
	private JLabel lblYouHaveOrdered = new JLabel("You have ordered: ");
	private JLabel orderInfolbl = new JLabel("");
	private JLabel lblCardNumber = new JLabel("Card Number: ");
	private JLabel lblCvc = new JLabel("CVC: ");
	private JTextField cardNumTF;
	private JTextField cvcTF;
	private JButton btnPayInCash = new JButton("Pay In Cash");
	private JButton btnPayByCard = new JButton("Pay by Card");
	private JButton btnBack = new JButton("Back");
	private JButton btnPayLater = new JButton("Pay Later");

	public int getCustID() {
		return custID;
	}

	public void setCustID(int custID) {
		this.custID = custID;
	}

	public int getItemID() {
		return itemID;
	}

	public void setItemID(int itemID) {
		this.itemID = itemID;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public void setCardDisabled() {
		btnPayByCard.setEnabled(false);
		cardNumTF.setEnabled(false);
		cvcTF.setEnabled(false);
	}
	
	public void setInfoText(String text) {
		orderInfolbl.setText(text);
	}

	/**
	 * Create the application.
	 * @throws Exception 
	 */
	public PaymentPage() throws Exception {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws Exception 
	 */
	private void initialize() throws Exception {
		orderDao=new OrderDAO();
		frmPaymentPage = new JFrame();
		frmPaymentPage.setTitle("Payment Page");
		frmPaymentPage.setBounds(100, 100, 500, 441);
		frmPaymentPage.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmPaymentPage.getContentPane().setLayout(null);
		

		btnPayByCard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!VerifyUtils.verifyCardNum(cardNumTF.getText())) {
					DialogMessage.showErrorDialog("Invalid card number info!");
					return;
				}else if(!VerifyUtils.verifyCvc(cvcTF.getText())) {
					DialogMessage.showErrorDialog("Invalid CVC info!");
					return;
				}
				try {
					createOrder("Paid");
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnPayByCard.setBounds(149, 193, 139, 40);
		frmPaymentPage.getContentPane().add(btnPayByCard);
		
		btnPayInCash.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					createOrder("Paid");
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
		
		btnPayInCash.setBounds(149, 258, 139, 40);
		frmPaymentPage.getContentPane().add(btnPayInCash);
		
		lblCardNumber.setBounds(38, 93, 110, 30);
		frmPaymentPage.getContentPane().add(lblCardNumber);
		
		cardNumTF = new JTextField();
		cardNumTF.setBounds(149, 93, 242, 30);
		frmPaymentPage.getContentPane().add(cardNumTF);
		cardNumTF.setColumns(10);
		
		cvcTF = new JTextField();
		cvcTF.setColumns(10);
		cvcTF.setBounds(149, 136, 97, 30);
		frmPaymentPage.getContentPane().add(cvcTF);
		
		lblCvc.setBounds(38, 136, 110, 30);
		frmPaymentPage.getContentPane().add(lblCvc);
		
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmPaymentPage.dispose();
			}
		});
		btnBack.setBounds(329, 341, 139, 40);
		
		frmPaymentPage.getContentPane().add(btnBack);
		lblYouHaveOrdered.setBounds(38, 32, 144, 30);
		
		frmPaymentPage.getContentPane().add(lblYouHaveOrdered);
		orderInfolbl.setBounds(196, 32, 195, 30);
		
		frmPaymentPage.getContentPane().add(orderInfolbl);
		btnPayLater.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					createOrder(OrderDAO.NOT_PAID);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnPayLater.setBounds(14, 341, 139, 40);
		
		frmPaymentPage.getContentPane().add(btnPayLater);
		
		//center window
		frmPaymentPage.setLocationRelativeTo(null);
		frmPaymentPage.setVisible(true);
	}
	
	private void createOrder(String payment) throws SQLException {
		TBOrders order=new TBOrders();
		order.setCustId(custID);
		order.setItemId(itemID);
		order.setNumber(amount);
		order.setOrderType(orderType);
		order.setPayment(payment);
		int result=orderDao.orderInsert(order);
		if(result>0) {
			DialogMessage.showInfoDialog("Order Created!");
			frmPaymentPage.dispose();
			return;
		}else {
			DialogMessage.showErrorDialog("Failed to create the order!");
		}
	}
}
