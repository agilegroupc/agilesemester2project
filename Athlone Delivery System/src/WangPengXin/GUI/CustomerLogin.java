package WangPengXin.GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import StartupGUI.TempSartup;
import WangPengXin.DialogMessage;
import WangPengXin.VerifyUtils;
import WangPengXin.DB.DAO.CustomerDAO;

import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class CustomerLogin {

	private JFrame frmDd;
	private JTextField EmailTF;
	private JPasswordField passwordField;
	private CustomerDAO cusDao;
	JButton btnBack = new JButton("Back");

	public static void main(String[] args) throws Exception {
		CustomerLogin window = new CustomerLogin();
	}

	public CustomerLogin() throws Exception {
		initialize();
	}

	private void initialize() throws Exception {
		cusDao = new CustomerDAO();
		frmDd = new JFrame();
		frmDd.setTitle("Customer Log in Page");
		frmDd.setResizable(false);
		frmDd.setBounds(100, 100, 577, 510);
		frmDd.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmDd.getContentPane().setLayout(null);
		
		JLabel emailLabel = new JLabel("Email: ");
		emailLabel.setBounds(75, 87, 97, 32);
		frmDd.getContentPane().add(emailLabel);
		
		JLabel PasswordLabel = new JLabel("Password: ");
		PasswordLabel.setBounds(75, 162, 97, 32);
		frmDd.getContentPane().add(PasswordLabel);
		
		EmailTF = new JTextField();
		EmailTF.setBounds(211, 86, 176, 35);
		frmDd.getContentPane().add(EmailTF);
		EmailTF.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(211, 162, 176, 32);
		frmDd.getContentPane().add(passwordField);
		
		JButton Loginbtn = new JButton("Log in");
		Loginbtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String email = EmailTF.getText();
					String pass = passwordField.getText();
					if (email.trim().equals("") || pass.trim().equals("")) {
						DialogMessage.showWarningDialog("Empty log in info!!!");
						return;
					}
					if (!VerifyUtils.verifyEmail(email)) {
						DialogMessage.showWarningDialog("Invalid email format!!!");
						return;
					}
					if (!VerifyUtils.verifyPasswordFormat(pass)) {
						DialogMessage.showWarningDialog("Invalid password format!!!");
						return;
					}
					if (pass.equals(cusDao.queryPassword(email))) {
						int id = cusDao.queryID(email);
						cusDao.close();
						CustomerMainPage custMain = new CustomerMainPage();
						custMain.setCustID(id);
						frmDd.dispose();
					} else if (cusDao.queryPassword(email) != null) {
						DialogMessage.showWarningDialog("Wrong password!!!");
						return;
					} else {
						DialogMessage.showWarningDialog("Acccount does not exist!!!");
						return;
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		Loginbtn.setBounds(81, 271, 122, 27);
		frmDd.getContentPane().add(Loginbtn);
		
		JButton Registerbtn = new JButton("Register");
		Registerbtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					new CustomerRegister();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				frmDd.dispose();
			}
		});
		Registerbtn.setBounds(245, 271, 122, 27);
		frmDd.getContentPane().add(Registerbtn);

		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TempSartup objCall = new TempSartup();
				frmDd.dispose();			}
		});
		btnBack.setBounds(419, 274, 85, 21);
		frmDd.getContentPane().add(btnBack);
		
		//center window
		frmDd.setLocationRelativeTo(null); 
		
		frmDd.setVisible(true);
	}
}
