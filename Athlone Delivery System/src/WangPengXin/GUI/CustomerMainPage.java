package WangPengXin.GUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.sql.SQLException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

import WangPengXin.DialogMessage;
import WangPengXin.VerifyUtils;
import WangPengXin.DB.DBUtils;
import WangPengXin.DB.DAO.CustomerDAO;
import WangPengXin.DB.DAO.NewsagentDAO;
import WangPengXin.DB.DAO.OrderDAO;

public class CustomerMainPage {

	private int custID = 0;
	private String orderID="";
	private double fee = 0;
	private int status = 1;
	private CustomerDAO cusDao;
	private OrderDAO orderDao;
	private NewsagentDAO newDao;
	private JFrame frmCustomerMainPage;

	private JLabel lblWellcome = new JLabel("Wellcome,");
	private JLabel custNamelbl = new JLabel("");
	private JLabel lblAddress = new JLabel("Address: ");
	private JLabel addresslbl = new JLabel("");
	private JLabel lblItem = new JLabel("Item: ");
	private JLabel lblAmount = new JLabel("Amount: ");
	private JLabel lblAvailableStatus = new JLabel("Available Status: ");
	private JLabel statuslbl = new JLabel("");
	private JLabel lblCustomerService = new JLabel("Customer Service: ");
	private JLabel contactNumlbl = new JLabel("");

	private JTextArea addressTA = new JTextArea();
	Integer[] amount = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	String[] item = { "newspaper", "magazine", "jouranl" };
	private JComboBox<String> itemBox = new JComboBox(item);
	private JComboBox<Integer> amountBox = new JComboBox(amount);
	
	private JScrollPane scrollPane;
	private static QueryTableModel TableModel = new QueryTableModel();
	private Border lineBorder= BorderFactory.createEtchedBorder(15, Color.red, Color.black);
	// Add the models to JTabels
	private JTable TableofDBContents = new JTable(TableModel);

	private JButton btnUpdateAddress = new JButton("update address");
	private JButton btnChangeStatus = new JButton("Change Status");
	private JButton btnPlaceOnetimeOrder = new JButton("Place one-time order");
	private JButton btnMothlySubscribe = new JButton("Mothly Subscribe");
	private JButton btnCancelOrder = new JButton("Cancel Order");
	JButton btnLogOut = new JButton("Log Out");

	/**
	 * Create the application.
	 * @throws Exception 
	 */
	public CustomerMainPage() throws Exception {
		initialize();
	}

	public int getCustID() {
		return custID;
	}

	public void setCustID(int custID) {
		this.custID = custID;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public double getFee() {
		double number = amountBox.getSelectedIndex() + 1;
		double price = 0;
		if (itemBox.getSelectedIndex() == 0) {
			price = 0.5;
		} else if (itemBox.getSelectedIndex() == 1) {
			price = 10;
		} else if (itemBox.getSelectedIndex() == 2) {
			price = 20;
		}
		fee = number * price;
		return fee;
	}

	public void setFee(double fee) {
		this.fee = fee;
	}

	/**
	 * Launch the application.
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		CustomerMainPage window = new CustomerMainPage();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws Exception 
	 */
	private void initialize() throws Exception {
		cusDao = new CustomerDAO();
		orderDao=  new OrderDAO();
		newDao = new NewsagentDAO();
		frmCustomerMainPage = new JFrame();
		frmCustomerMainPage.setTitle("Customer Main Page");
		frmCustomerMainPage.setBounds(100, 100, 948, 848);
		frmCustomerMainPage.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCustomerMainPage.getContentPane().setLayout(null);

		lblWellcome.setHorizontalAlignment(SwingConstants.CENTER);
		lblWellcome.setBounds(14, 13, 105, 46);
		frmCustomerMainPage.getContentPane().add(lblWellcome);

		custNamelbl.setBounds(119, 13, 132, 46);
		frmCustomerMainPage.getContentPane().add(custNamelbl);

		lblAddress.setHorizontalAlignment(SwingConstants.CENTER);
		lblAddress.setBounds(14, 72, 105, 38);
		frmCustomerMainPage.getContentPane().add(lblAddress);
		addressTA.setBounds(471, 60, 266, 70);
		frmCustomerMainPage.getContentPane().add(addressTA);
		btnUpdateAddress.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (addressTA.getText().trim().equals("")) {
					DialogMessage.showErrorDialog("Empty address!");
					return;
				}
				if (!VerifyUtils.verifyAddress(addressTA.getText())) {
					DialogMessage.showErrorDialog("Invalid address!");
					return;
				}
				int num = 0;
				try {
					num = cusDao.updateAddress(addressTA.getText(), custID);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				if (num > 0) {
					refresh();
					DialogMessage.showInfoDialog("Update successful!");
					return;
				}
				DialogMessage.showErrorDialog("Update failed!");
			}
		});

		btnUpdateAddress.setBounds(751, 92, 159, 38);
		frmCustomerMainPage.getContentPane().add(btnUpdateAddress);

		addresslbl.setHorizontalAlignment(SwingConstants.LEFT);
		addresslbl.setBounds(119, 72, 338, 38);
		frmCustomerMainPage.getContentPane().add(addresslbl);

		itemBox.setBounds(119, 176, 105, 24);
		frmCustomerMainPage.getContentPane().add(itemBox);

		lblItem.setHorizontalAlignment(SwingConstants.CENTER);
		lblItem.setBounds(14, 176, 90, 24);

		frmCustomerMainPage.getContentPane().add(lblItem);
		btnPlaceOnetimeOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PaymentPage payment=null;
				try {
					payment = new PaymentPage();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				payment.setCustID(custID);
				payment.setItemID(itemBox.getSelectedIndex() + 1);
				payment.setAmount(amountBox.getSelectedIndex() + 1);
				payment.setOrderType("one-time");
				payment.setInfoText((amountBox.getSelectedIndex() + 1) + "  " + itemBox.getSelectedItem()
						+ ",  total:  " + getFee() + "��");
				payment.setCardDisabled();
				System.out.println(itemBox.getSelectedItem().toString());
			}
		});

		btnPlaceOnetimeOrder.setBounds(486, 175, 205, 27);
		frmCustomerMainPage.getContentPane().add(btnPlaceOnetimeOrder);

		btnMothlySubscribe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PaymentPage payment=null;
				try {
					payment = new PaymentPage();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				payment.setCustID(custID);
				payment.setItemID(itemBox.getSelectedIndex() + 1);
				payment.setAmount(amountBox.getSelectedIndex() + 1);
				payment.setOrderType("monthly subscription");
				payment.setInfoText((amountBox.getSelectedIndex() + 1) + "  " + itemBox.getSelectedItem()
						+ ",  total:  " + getFee() + "��");
				System.out.println(itemBox.getSelectedItem().toString());
			}
		});
		btnMothlySubscribe.setBounds(705, 175, 205, 27);
		frmCustomerMainPage.getContentPane().add(btnMothlySubscribe);

		lblAmount.setHorizontalAlignment(SwingConstants.CENTER);
		lblAmount.setBounds(252, 176, 90, 24);
		frmCustomerMainPage.getContentPane().add(lblAmount);

		amountBox.setBounds(344, 176, 54, 24);
		frmCustomerMainPage.getContentPane().add(amountBox);

		lblAvailableStatus.setHorizontalAlignment(SwingConstants.CENTER);
		lblAvailableStatus.setBounds(14, 125, 152, 38);
		frmCustomerMainPage.getContentPane().add(lblAvailableStatus);

		statuslbl.setHorizontalAlignment(SwingConstants.LEFT);
		statuslbl.setBounds(180, 123, 115, 38);
		frmCustomerMainPage.getContentPane().add(statuslbl);

		btnChangeStatus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if (statuslbl.getText().equals("Available")) {
						status=0;
						int result=cusDao.updateStatus(custID, status);
						if(result>0) {
							statuslbl.setText("Unavailable");
							DialogMessage.showInfoDialog("Updated status successfully!");
							return;
						}
					}else {
						status=1;
						int result=cusDao.updateStatus(custID, status);
						if(result>0) {
							statuslbl.setText("Available");
							DialogMessage.showInfoDialog("Updated status successfully!");
							return;
						}
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		btnChangeStatus.setBounds(287, 131, 159, 27);
		frmCustomerMainPage.getContentPane().add(btnChangeStatus);
		
		TableofDBContents.setPreferredScrollableViewportSize(new Dimension(900, 300));
		scrollPane = new JScrollPane(TableofDBContents, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBorder(BorderFactory.createTitledBorder(lineBorder, "Orders Information"));
		scrollPane.setBackground(Color.LIGHT_GRAY);
		scrollPane.setBounds(14, 213, 898, 353);
		frmCustomerMainPage.getContentPane().add(scrollPane);
		
		lblCustomerService.setHorizontalAlignment(SwingConstants.CENTER);
		lblCustomerService.setBounds(14, 750, 170, 38);
		frmCustomerMainPage.getContentPane().add(lblCustomerService);
		
		contactNumlbl.setHorizontalAlignment(SwingConstants.CENTER);
		contactNumlbl.setBounds(180, 750, 170, 38);
		frmCustomerMainPage.getContentPane().add(contactNumlbl);
		
		btnLogOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmCustomerMainPage.dispose();
				try {
					new CustomerLogin();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		btnLogOut.setBounds(778, 750, 132, 38);
		frmCustomerMainPage.getContentPane().add(btnLogOut);
		
		btnCancelOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int num=0;
				if(orderID.equals("")) {
					DialogMessage.showWarningDialog("Please choose an order at first!!!");
					return;
				}
				try {
					num=orderDao.updateOrderStatus(orderID);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				if(num>0)
					DialogMessage.showInfoDialog("Cancel successful!");
				refresh();
			}
		});
		btnCancelOrder.setBounds(14, 579, 170, 38);
		frmCustomerMainPage.getContentPane().add(btnCancelOrder);
		
		TableofDBContents.addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				Object o = e.getSource();
				if (o instanceof JTable) {
					JTable t = (JTable) o;
					QueryTableModel qtb = (QueryTableModel) t.getModel();
					orderID = (String) qtb.getValueAt(t.getSelectedRow(), 0);
				}
			}
		});
		
		refresh();
		
		// center window
		frmCustomerMainPage.setLocationRelativeTo(null);
		frmCustomerMainPage.setVisible(true);
		
		frmCustomerMainPage.addWindowFocusListener(new WindowFocusListener() {
			
			@Override
			public void windowLostFocus(WindowEvent e) {
				refresh();
			}
			
			@Override
			public void windowGainedFocus(WindowEvent e) {
				refresh();
			}
		});
	}

	private void refresh() {
		try {
			String[] name = cusDao.queryName(custID);
			String addr = cusDao.queryAddress(custID);
			addresslbl.setText(addr);
			TableModel.refreshFromDB(new DBUtils().getConnection().createStatement(), 
					OrderDAO.orderDetails+custID);
			contactNumlbl.setText(newDao.queryPhone("mikewang1997@qq.com"));
			if (cusDao.queryStatus(custID) == 1) {
				statuslbl.setText("Available");
			} else {
				statuslbl.setText("Unavailable");
			}
			custNamelbl.setText(name[0] + " " + name[1]);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
}
