package WangPengXin.Test;

import static org.junit.Assert.*;

import org.junit.Test;

import WangPengXin.DB.DAO.NewsagentDAO;

public class TestGetContactNumber {

	@Test
	public void testQueryPhone() throws Exception {
		NewsagentDAO newsD=new NewsagentDAO();
		newsD.setConnection("com.mysql.cj.jdbc.Driver",
				"jdbc:mysql://localhost:3306/athlone_delivery_system?serverTimezone=UTC", 
				"root", "mike");
		assertEquals("0876431923",newsD.queryPhone("mikewang1997@qq.com"));
	}

}
