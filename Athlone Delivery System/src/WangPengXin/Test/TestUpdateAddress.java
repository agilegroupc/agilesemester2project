package WangPengXin.Test;

import static org.junit.Assert.*;

import org.junit.Test;

import WangPengXin.DB.DAO.CustomerDAO;

public class TestUpdateAddress {

	@Test
	public void test() throws Exception {
		CustomerDAO cusDao=new CustomerDAO();
		cusDao.setConnection("com.mysql.cj.jdbc.Driver",
				"jdbc:mysql://localhost:3306/athlone_delivery_system?serverTimezone=UTC", 
				"root", "mike");
			assertEquals(1, cusDao.updateAddress("Wellmount students", 1));
	}

}
