package WangPengXin.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.sql.SQLException;

import org.junit.Test;

import WangPengXin.DB.DBUtils;
import WangPengXin.DB.DAO.OrderDAO;
import WangPengXin.GUI.*;

public class testPrintDeliveryInfo {

	@Test
	public void testRefreshFromDB1() throws Exception {
		QueryTableModel TableModel=new QueryTableModel();
		TableModel.refreshFromDB(new DBUtils().getConnection().createStatement(), 
				OrderDAO.orderDetails+"1");
		assertNotEquals(0, TableModel.getRowCount());
	}
	
	@Test
	public void testRefreshFromDB2() throws Exception {
		QueryTableModel TableModel=new QueryTableModel();
		TableModel.refreshFromDB(new DBUtils().getConnection().createStatement(), 
				OrderDAO.orderDetails+"2");
		assertEquals(0, TableModel.getRowCount());
	}

}
