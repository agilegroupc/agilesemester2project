package WangPengXin.Test;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;

import org.junit.Test;

import WangPengXin.DB.DBUtils;
import orders.QueryTableModel;

public class testShowOrders {
	
	@Test
	public void testShowActiveOrders() throws Exception {
		QueryTableModel TableModel = new QueryTableModel();
		TableModel.refreshFromDB(new DBUtils().getConnection().createStatement(), 
				"select * from orders where order_status!=\"Canceled\"");
		assertNotEquals(0, TableModel.getRowCount());
	}

	@Test
	public void testShowALLOrders() throws Exception {
		QueryTableModel TableModel = new QueryTableModel();
		TableModel.refreshFromDB(new DBUtils().getConnection().createStatement(), 
				"select * from orders;");
		assertNotEquals(0, TableModel.getRowCount());
	}
	
}
