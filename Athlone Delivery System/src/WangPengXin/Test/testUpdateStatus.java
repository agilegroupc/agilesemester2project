package WangPengXin.Test;

import static org.junit.Assert.*;

import org.junit.Test;

import WangPengXin.DB.DAO.CustomerDAO;

public class testUpdateStatus {
	
	@Test
	public void testUpdateStatus() throws Exception {
		CustomerDAO c=new CustomerDAO();
		assertEquals(1, c.updateStatus(1, 1));
	}
	

	@Test
	public void testQueryStatus() throws Exception {
		CustomerDAO c=new CustomerDAO();
		int s=c.queryStatus(1);
		assertNotEquals(-1, s);
	}

}
