package WangPengXin.Test;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Test;

import WangPengXin.DB.DAO.OrderDAO;

public class testCancelOrder {

	@Test
	public void testUpdateOrderStatus() throws Exception {
		OrderDAO o = new OrderDAO();
		assertEquals(1, o.updateOrderStatus("1"));
	}

	@Test(expected=SQLException.class)
	public void testUpdateOrderStatus2() throws Exception {
		OrderDAO o = new OrderDAO();
		o.updateOrderStatus("ads");
	}

}
