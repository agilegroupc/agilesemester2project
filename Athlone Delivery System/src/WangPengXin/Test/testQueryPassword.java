package WangPengXin.Test;

import static org.junit.Assert.*;

import org.junit.Test;

import WangPengXin.DB.DAO.CustomerDAO;

public class testQueryPassword {

	@Test
	public void test1() throws Exception {
		CustomerDAO cusDao=new CustomerDAO();
		cusDao.setConnection("com.mysql.cj.jdbc.Driver",
				"jdbc:mysql://localhost:3306/athlone_delivery_system?serverTimezone=UTC", 
				"root", "mike");
		assertEquals("abc123", cusDao.queryPassword("mikewang@qq.com"));
	}
	
	@Test
	public void test2() throws Exception {
		CustomerDAO cusDao=new CustomerDAO();
		cusDao.setConnection("com.mysql.cj.jdbc.Driver",
				"jdbc:mysql://localhost:3306/athlone_delivery_system?serverTimezone=UTC", 
				"root", "mike");
		assertNotEquals("asds", cusDao.queryPassword("mikewang@qq.com"));
	}

	@Test
	public void test3() throws Exception {
		CustomerDAO cusDao=new CustomerDAO();
		cusDao.setConnection("com.mysql.cj.jdbc.Driver",
				"jdbc:mysql://localhost:3306/athlone_delivery_system?serverTimezone=UTC", 
				"root", "mike");
		assertNotEquals("abc123", cusDao.queryPassword("mikewangsd222@qq.com"));
	}
}
