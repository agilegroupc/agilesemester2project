package WangPengXin.Test;

import static org.junit.Assert.*;

import org.junit.Test;

import WangPengXin.VerifyUtils;

public class TestVerifyPayment {

	@Test
	public void testVerifyCardNum1() {
		assertEquals(true, VerifyUtils.verifyCardNum("1234567812345678"));
	}
	
	@Test
	public void testVerifyCardNum2() {
		assertEquals(false, VerifyUtils.verifyCardNum("12345678123456781"));
	}
	
	@Test
	public void testVerifyCardNum3() {
		assertEquals(false, VerifyUtils.verifyCardNum("123456781234567"));
	}
	
	@Test
	public void testVerifyCardNum4() {
		assertEquals(false, VerifyUtils.verifyCardNum("sdfsd223"));
	}

	@Test
	public void testVerifyCvc1() {
		assertEquals(true, VerifyUtils.verifyCvc("123"));
	}

	@Test
	public void testVerifyCvc2() {
		assertEquals(false, VerifyUtils.verifyCvc("12"));
	}
	
	@Test
	public void testVerifyCvc3() {
		assertEquals(false, VerifyUtils.verifyCvc("1234"));
	}
	
	@Test
	public void testVerifyCvc4() {
		assertEquals(false, VerifyUtils.verifyCvc("dds22"));
	}
}
