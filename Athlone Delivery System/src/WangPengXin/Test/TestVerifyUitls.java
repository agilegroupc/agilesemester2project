package WangPengXin.Test;

import static org.junit.Assert.*;

import org.junit.Test;

import WangPengXin.VerifyUtils;

public class TestVerifyUitls {

	@Test
	public void testVerifyName1() {
		assertEquals(true, VerifyUtils.verifyName("adxcdfcglsdksls"));
	}

	@Test
	public void testVerifyAge() {
		assertEquals(true, VerifyUtils.verifyAge(18));
	}

	@Test
	public void testVerifyGender() {
		assertEquals(false, VerifyUtils.verifyGender("ssdads"));
	}

	@Test
	public void testVerifyAddress() {
		assertEquals(true, VerifyUtils.verifyAddress("Wellmount student village"));
	}

	@Test
	public void testVerifyPhone() {
		assertEquals(true, VerifyUtils.verifyPhone("123456789"));
	}

	@Test
	public void testVerifyEmail() {
		assertEquals(false, VerifyUtils.verifyEmail("ssdads"));
	}

	@Test
	public void testVerifyPasswordFormat() {
		assertEquals(true, VerifyUtils.verifyPasswordFormat("123456"));
	}

}
