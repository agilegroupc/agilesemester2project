package WangPengXin.DB.Table;

public class TBCustomer {
	private int customerId;
	private String firstName;
	private String lastName;
	private int age; 
	private String gender; 
	private String address;
	private String phone;
	private String email; 
	private String password;
	private String available_status;
	private String billing_status;
	
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAvailable_status() {
		return available_status;
	}
	public void setAvailable_status(String available_status) {
		this.available_status = available_status;
	}
	public String getBilling_status() {
		return billing_status;
	}
	public void setBilling_status(String billing_status) {
		this.billing_status = billing_status;
	}
	
}
