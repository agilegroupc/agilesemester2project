package WangPengXin.DB;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class DBUtils {
	private static String driver = null;
	private static String url = null;
	private static String username = null;
	private static String password = null;
	private Connection conn;
	private PreparedStatement ps;
	private ResultSet rs;

	/*
	 * lazy loading DBConnection object don't initialize before invoking this method
	 */
	public static DBUtils getDBUtils() throws Exception {
		return new DBUtils();
	}

	public DBUtils() throws Exception {
		FileInputStream fin = new FileInputStream("./src/WangPengXin/DB/db.properties");
		Properties prop = new Properties();
		prop.load(fin);

		driver = prop.getProperty("driver");
		url = prop.getProperty("url");
		username = prop.getProperty("username");
		password = prop.getProperty("password");
		Class.forName(driver);
		conn = DriverManager.getConnection(url, username, password);
	}

	public void setConnection(String driver, String url, String username, String password) throws Exception {
		Class.forName(driver);
		conn = DriverManager.getConnection(url, username, password);
	}

	/**
	 * @Method: getConnection
	 * @return Connection
	 * @throws SQLException
	 */
	public Connection getConnection() throws Exception {
		return conn;
	}

	/**
	 * @throws SQLException
	 * @Method: release
	 * @Description: release resources including Connection, Statement, ResultSet
	 *               objects
	 */
	public void release(Connection conn, Statement st, ResultSet rs) throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (st != null) {
			st.close();
		}
		if (conn != null) {
			conn.close();
		}
	}

}
