package WangPengXin.DB.DAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import WangPengXin.DialogMessage;
import WangPengXin.DB.DBUtils;
import WangPengXin.DB.Table.TBCustomer;

public class CustomerDAO {

	private static DBUtils dbc;
	private static Connection conn = null;
	private static PreparedStatement st = null;
	private static ResultSet rs = null;

	public CustomerDAO() throws Exception {
		dbc = DBUtils.getDBUtils();
		conn = dbc.getConnection();
	}

	public void setConnection(String driver, String url, String username, String password) throws Exception {
		dbc.setConnection(driver, url, username, password);
		conn = dbc.getConnection();
	}

	public void close() throws SQLException {
		dbc.release(conn, st, rs);
	}

	public int customerInsert(TBCustomer customer) throws SQLException {
		int num = -2;
		String sql = "insert into customers(firstName,lastName,age,gender,address,phone"
				+ ",email,password) values(?,?,?,?,?,?,?,?)";
		st = conn.prepareStatement(sql);
		// index starts from 1
		st.setString(1, customer.getFirstName());
		st.setString(2, customer.getLastName());
		st.setInt(3, customer.getAge());
		st.setString(4, customer.getGender());
		st.setString(5, customer.getAddress());
		st.setString(6, customer.getPhone());
		st.setString(7, customer.getEmail());
		st.setString(8, customer.getPassword());
		// execute and return number of rows that take effect
		num = st.executeUpdate();
		return num;
	}

	public int updateAddress(String address, int id) throws Exception {
		int num = -1;
		String sql = "update customers set address=? where customer_id=?";
		st = conn.prepareStatement(sql);
		st.setString(1, address);
		st.setInt(2, id);
		num = st.executeUpdate();
		return num;
	}

	public int updateStatus(int id, int status) throws Exception {
		int num = -1;
		String sql = "update customers set available_status=? where customer_id=?";
		st = conn.prepareStatement(sql);
		st.setInt(1, status);
		st.setInt(2, id);
		num = st.executeUpdate();
		return num;
	}
	
	public int queryStatus(int id) throws Exception {
		int status = -1;
		String sql = "select available_status from customers where customer_id=?";
		st = conn.prepareStatement(sql);
		st.setInt(1, id);
		rs = st.executeQuery();
		while (rs.next()) {
			status = rs.getInt(1);
		}
		return status;
	}

	public String queryPassword(String email) throws SQLException {
		String pass = null;
		String sql = "select password from customers where email=?";
		st = conn.prepareStatement(sql);
		st.setString(1, email);
		rs = st.executeQuery();
		while (rs.next()) {
			pass = rs.getString(1);
		}
		return pass;
	}

	public int queryID(String email) throws SQLException {
		int id = -1;
		String sql = "select customer_id from customers where email=?";
		st = conn.prepareStatement(sql);
		st.setString(1, email);
		rs = st.executeQuery();
		while (rs.next()) {
			id = rs.getInt(1);
		}
		return id;
	}

	public String queryAddress(int id) throws SQLException {
		String addr = null;
		String sql = "select address from customers where customer_id=?";
		st = conn.prepareStatement(sql);
		st.setInt(1, id);
		rs = st.executeQuery();
		while (rs.next()) {
			addr = rs.getString(1);
		}
		return addr;
	}

	public String[] queryName(int id) throws SQLException {
		String[] name = new String[2];
		String sql = "select firstName,lastName from customers where customer_id=?";
		st = conn.prepareStatement(sql);
		st.setInt(1, id);
		rs = st.executeQuery();
		while (rs.next()) {
			name[0] = rs.getString(1);
			name[1] = rs.getString(2);
		}
		return name;
	}

}
