package WangPengXin.DB.DAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import WangPengXin.DB.DBUtils;
import WangPengXin.DB.Table.TBCustomer;
import WangPengXin.DB.Table.TBOrders;

public class OrderDAO {
	private static DBUtils dbc;
	private static Connection conn = null;
	private static PreparedStatement st = null;
	private static ResultSet rs = null;
	public static final String NOT_PAID="Not Paid";
	public static final String orderDetails = "select order_id, orders.item_id, "
			+ "item_name, cost as unit_price, order_type, numberof, order_date, order_status " + "from orders,items "
			+ "where orders.item_id=items.item_id and order_status!=\"Canceled\" and customer_id=";

	public OrderDAO() throws Exception {
		dbc = DBUtils.getDBUtils();
		conn = dbc.getConnection();
	}

	public void setConnection(String driver, String url, String username, String password) throws Exception {
		dbc.setConnection(driver, url, username, password);
		conn = dbc.getConnection();
	}

	public void close() throws SQLException {
		dbc.release(conn, st, rs);
	}

	public int orderInsert(TBOrders orders) throws SQLException {
		int num = -2;
		String sql = "insert into orders(item_id,customer_id,numberof,order_type,order_date,payment_method"
				+ ") values(?,?,?,?,NOW(),?)";
		st = conn.prepareStatement(sql);
		// index starts from 1
		st.setInt(1, orders.getItemId());
		st.setInt(2, orders.getCustId());
		st.setInt(3, orders.getNumber());
		st.setString(4, orders.getOrderType());
		st.setString(5, orders.getPayment());
		// execute and return number of rows that take effect
		num = st.executeUpdate();
		return num;
	}

	public int querySubscription() throws SQLException   {
		int num = -2;
		String sql = "select * from orders where order_type=2";
		st = conn.prepareStatement(sql);
		num = st.executeUpdate();
		return num;
	}
	
	public int updateOrderStatus(String orderID) throws SQLException {
		int num=-2;
		String sql="update orders set order_status=\"Canceled\" where order_id="+orderID;
		st = conn.prepareStatement(sql);
		num=st.executeUpdate();
		return num;
	}
	
	public int updateField(String field,String orderID,String content) throws SQLException {
		int num=-2;
		String sql="update orders set "+field+"=\""+content+"\" where order_id="+orderID;
		st = conn.prepareStatement(sql);
		num=st.executeUpdate();
		return num;
	}
}
