package WangPengXin.DB.DAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import WangPengXin.DialogMessage;
import WangPengXin.DB.DBUtils;
import WangPengXin.DB.Table.TBCustomer;

public class NewsagentDAO {

	private static DBUtils dbc;
	private static Connection conn=null ;
	private static PreparedStatement st=null;
	private static ResultSet rs=null;
	
	public NewsagentDAO() throws Exception {
			dbc=DBUtils.getDBUtils();
			conn = dbc.getConnection();
	}
	
	public void setConnection(String driver,String url,String username,String password) throws Exception {
			dbc.setConnection(driver, url, username, password);
			conn=dbc.getConnection();
	}
	
	public void close() throws SQLException {
		dbc.release(conn, st, rs);
	}
	
	public String queryPhone(String email) throws SQLException {
		String pass=null;
		String sql = "select phone from newsagent where email=?";
			st = conn.prepareStatement(sql);
			st.setString(1, email);
			rs=st.executeQuery();
			while(rs.next()) {
				pass=rs.getString(1);
			}
		return pass;
	}

}
