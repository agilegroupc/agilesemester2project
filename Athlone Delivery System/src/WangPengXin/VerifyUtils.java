package WangPengXin;

public class VerifyUtils {

	public static boolean verifyName(String name) {
		String regex="[a-zA-Z]{3,15}";
		return name.matches(regex);
	}

	public static boolean verifyAge(int age) {
		if (age >= 18 && age <= 100) {
			return true;
		}
		return false;
	}

	public static boolean verifyGender(String gender) {
		if(gender.equals("m" ) || gender.equals("f") || gender.equals("M") || gender.equals("F")) {
			return true;
		}
		return false;
	}

	public static boolean verifyAddress(String address) {
		//letters, numbers and spaces 
		String regex="[0-9a-zA-Z\\s+]{10,50}";
		return address.matches(regex);
	}

	public static boolean verifyPhone(String phone) {
		// 9 digits phone number
		String regex = "[0-9]{9}";
		return phone.matches(regex);
	}

	public static boolean verifyEmail(String email) {
		/*
		 * Email addresses must contain the @ character The left side of the @ box must
		 * start with a letter, an underscore, a number, and must have one To the left
		 * of the @ email box, except for the initial letters, other letters can be
		 * letters, underscores, Numbers, dots The right side of the @ must have a dot
		 * Between @ and. Must begin with a letter, underscore, number, and anything
		 * other than the initial letter can be a letter, underscore, number, dot. Dash
		 * - At least one letter after the dot, underscore, and begin with a number
		 */
		String regex = "(\\w+)([\\w+.-])*@(\\w+)([\\w+.-])*\\.\\w+";
		return email.matches(regex);
	}

	public static boolean verifyPasswordFormat(String password) {
		String regex="[0-9a-zA-Z]{6,18}";
		return password.matches(regex);
	}

	public static boolean verifyCardNum(String cardNum) {
		String regex="[0-9]{16}";
		return cardNum.matches(regex);
	}
	
	public static boolean verifyCvc(String cvc) {
		String regex="[0-9]{3}";
		return cvc.matches(regex);
	}
}
