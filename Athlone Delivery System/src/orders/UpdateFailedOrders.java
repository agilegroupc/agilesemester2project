package orders;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import AbhikRai.DialogMessage;
import AbhikRai.GUI.QueryModel;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class UpdateFailedOrders {

	String cmd = null;

	// DB Connectivity Attributes
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;

	private JFrame frame;
	QueryModel TableModel1 = new QueryModel();
	QueryModel TableModel2 = new QueryModel();
	private JTable table1 = new JTable(TableModel1);
	private JTable table2 = new JTable(TableModel2);
	private JTextField OrderIDTF;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UpdateFailedOrders window = new UpdateFailedOrders();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public UpdateFailedOrders() {
		initialize();
		TableModel1.call(2);
		TableModel2.call(5);
		frame.setVisible(true);

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		frame = new JFrame();
		frame.setBounds(100, 100, 762, 761);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.setLocationRelativeTo(null);
		frame.getContentPane().setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(24, 33, 684, 244);
		frame.getContentPane().add(scrollPane);

		JButton btnFailedDeliveries = new JButton("Failed Deliveries");
		btnFailedDeliveries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TableModel1.call(2);
				table1.setModel(TableModel1);
			}
		});
		btnFailedDeliveries.setBounds(24, 305, 159, 41);
		frame.getContentPane().add(btnFailedDeliveries);

		table1 = new JTable();
		table2 = new JTable();
		scrollPane.setViewportView(table1);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(24, 431, 684, 235);
		frame.getContentPane().add(scrollPane_1);
		scrollPane_1.setViewportView(table2);

		JButton btnFailedOrders = new JButton("Show All Orders");
		btnFailedOrders.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TableModel2.call(5);
				table2.setModel(TableModel2);
			}
		});
		btnFailedOrders.setBounds(24, 377, 159, 41);
		frame.getContentPane().add(btnFailedOrders);

		JLabel lblOrderId = new JLabel("Order ID: ");
		lblOrderId.setBounds(331, 317, 99, 16);
		frame.getContentPane().add(lblOrderId);

		OrderIDTF = new JTextField();
		OrderIDTF.setBounds(452, 314, 116, 22);
		frame.getContentPane().add(OrderIDTF);
		OrderIDTF.setColumns(10);

		JButton btnUpdateOrderStatus = new JButton("Fail Cancelled orders");
		btnUpdateOrderStatus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					String updateTemp = "UPDATE Orders SET " + " order_status ='Canceled"
							+ "' where order_id = " + OrderIDTF.getText();

					int num=TableModel2.changeTable(updateTemp);
					if(num>0)
						DialogMessage.showInfoDialog("Updated Successfully!");
			}
		});
		btnUpdateOrderStatus.setBounds(331, 366, 217, 33);
		frame.getContentPane().add(btnUpdateOrderStatus);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new OrdersManagementPage();
				frame.dispose();
			}
		});
		btnBack.setBounds(595, 674, 113, 27);
		frame.getContentPane().add(btnBack);

		frame.setLocationRelativeTo(null);
	}
}
