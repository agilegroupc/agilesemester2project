package orders;

import java.awt.EventQueue;

import javax.swing.JFrame;

import Suriya.newsagent.NewsAgentMainPage;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class OrdersManagementPage {

	private JFrame frmOrdersManagement;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OrdersManagementPage window = new OrdersManagementPage();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public OrdersManagementPage() {
		initialize();
		frmOrdersManagement.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmOrdersManagement = new JFrame();
		frmOrdersManagement.setTitle("Orders Management");
		frmOrdersManagement.setBounds(100, 100, 452, 467);
		frmOrdersManagement.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmOrdersManagement.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("Assgin Orders");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new AssignOrdertoDeliveryDocket();
				frmOrdersManagement.dispose();
			}
		});
		btnNewButton.setBounds(102, 65, 206, 43);
		frmOrdersManagement.getContentPane().add(btnNewButton);
		
		JButton btnUpdateFailedOrders = new JButton("Update Failed Orders");
		btnUpdateFailedOrders.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new UpdateFailedOrders();
				frmOrdersManagement.dispose();
			}
		});
		btnUpdateFailedOrders.setBounds(102, 164, 206, 43);
		frmOrdersManagement.getContentPane().add(btnUpdateFailedOrders);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new NewsAgentMainPage();
				frmOrdersManagement.dispose();
			}
		});
		btnBack.setBounds(102, 264, 206, 43);
		frmOrdersManagement.getContentPane().add(btnBack);
		
		frmOrdersManagement.setLocationRelativeTo(null);
	}

}
