package orders;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JTextField;

import AbhikRai.DialogMessage;
import AbhikRai.GUI.QueryModel;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;

public class AssignOrdertoDeliveryDocket {
	
	String cmd = null;

	private JFrame frame;
	QueryModel TableModel = new QueryModel();
	private JTable table = new JTable(TableModel);

	private JFrame frmAssignOrdertoDeliveryDocket;
	private JTextField DeliveryPersonIDTF;
	private JTextField OrderIDTF;
	private JTextField DeliveryStatusTF;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AssignOrdertoDeliveryDocket window = new AssignOrdertoDeliveryDocket();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AssignOrdertoDeliveryDocket() {
		initialize();
		TableModel.call(5);
		table.setModel(TableModel);
		frmAssignOrdertoDeliveryDocket.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAssignOrdertoDeliveryDocket = new JFrame();
		frmAssignOrdertoDeliveryDocket.setBounds(100, 100, 719, 520);
		frmAssignOrdertoDeliveryDocket.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAssignOrdertoDeliveryDocket.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 13, 677, 195);
		frmAssignOrdertoDeliveryDocket.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JLabel lblDeliveryPersonId = new JLabel("Delivery Person ID: ");
		lblDeliveryPersonId.setBounds(44, 251, 128, 27);
		frmAssignOrdertoDeliveryDocket.getContentPane().add(lblDeliveryPersonId);
		
		JLabel lblOrderId = new JLabel("Order ID: ");
		lblOrderId.setBounds(44, 303, 128, 27);
		frmAssignOrdertoDeliveryDocket.getContentPane().add(lblOrderId);
		
		DeliveryPersonIDTF = new JTextField();
		DeliveryPersonIDTF.setBounds(184, 253, 116, 22);
		frmAssignOrdertoDeliveryDocket.getContentPane().add(DeliveryPersonIDTF);
		DeliveryPersonIDTF.setColumns(10);
		
		OrderIDTF = new JTextField();
		OrderIDTF.setBounds(184, 305, 116, 22);
		frmAssignOrdertoDeliveryDocket.getContentPane().add(OrderIDTF);
		OrderIDTF.setColumns(10);
		
		JLabel lblOrderStatus = new JLabel("Delivery Status: ");
		lblOrderStatus.setBounds(44, 356, 97, 16);
		frmAssignOrdertoDeliveryDocket.getContentPane().add(lblOrderStatus);
		
		DeliveryStatusTF = new JTextField();
		DeliveryStatusTF.setBounds(184, 353, 116, 22);
		frmAssignOrdertoDeliveryDocket.getContentPane().add(DeliveryStatusTF);
		DeliveryStatusTF.setColumns(10);
		
		JButton btnAssignOrder = new JButton("Assign Order");
		btnAssignOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					String updateTemp ="UPDATE DeliveryDocket SET " +
					" order_id = "+OrderIDTF.getText()+
					", deliverydocket_status ='" +DeliveryStatusTF.getText()+
					"' where dpId = "+DeliveryPersonIDTF.getText();

					int num=TableModel.changeTable(updateTemp);
					if(num>0)
						DialogMessage.showInfoDialog("Updated Successfully!");
			}
		});
		btnAssignOrder.setBounds(44, 404, 128, 36);
		frmAssignOrdertoDeliveryDocket.getContentPane().add(btnAssignOrder);
		
		JButton btnNewButton = new JButton("Cancel Order");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					String updateTemp ="UPDATE DeliveryDocket SET " +
					" order_id = "+OrderIDTF.getText()+
					", deliverydocket_status = 0 "+
					" where dpId = "+DeliveryPersonIDTF.getText();
					
					int num=TableModel.changeTable(updateTemp);
					if(num>0)
						DialogMessage.showInfoDialog("Updated Successfully!");
			}
		});
		btnNewButton.setBounds(184, 404, 116, 36);
		frmAssignOrdertoDeliveryDocket.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Back");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new OrdersManagementPage();
				frmAssignOrdertoDeliveryDocket.dispose();
			}
		});
		btnNewButton_1.setBounds(553, 404, 97, 36);
		frmAssignOrdertoDeliveryDocket.getContentPane().add(btnNewButton_1);
		
		frmAssignOrdertoDeliveryDocket.setLocationRelativeTo(null);
	}
}
