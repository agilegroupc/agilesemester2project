package orders;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.Border;

import WangPengXin.DialogMessage;
import WangPengXin.DB.DBUtils;
import WangPengXin.DB.DAO.OrderDAO;


public class NewsagentOrdersGUI {

	private String orderId="";
	private String updateField="";
	private OrderDAO orderDao;
	private JFrame frmNewsagent;
	private JScrollPane scrollPane;
	private static QueryTableModel TableModel = new QueryTableModel();
	private Border lineBorder= BorderFactory.createEtchedBorder(15, Color.red, Color.black);
	// Add the models to JTabels
	private JTable TableofDBContents = new JTable(TableModel);
	private JButton btnBack;
	private JButton btnListActiveOrders;
	private JButton btnRefresh;
	private JTextField updateInfoTF;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NewsagentOrdersGUI window = new NewsagentOrdersGUI();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @throws Exception 
	 */
	public NewsagentOrdersGUI() throws Exception {
		initialize();
		frmNewsagent.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws Exception 
	 */
	private void initialize() throws Exception {
		orderDao=new OrderDAO();
		frmNewsagent = new JFrame();
		frmNewsagent.setTitle("NewsAgent Order Management");
		frmNewsagent.setBounds(100, 100, 1017, 743);
		frmNewsagent.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmNewsagent.getContentPane().setLayout(null);
		
		TableofDBContents.setPreferredScrollableViewportSize(new Dimension(900, 300));
		scrollPane = new JScrollPane(TableofDBContents, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBorder(BorderFactory.createTitledBorder(lineBorder, "Customer Order Information"));
		scrollPane.setBackground(Color.LIGHT_GRAY);
		scrollPane.setBounds(42, 13, 912, 438);
		frmNewsagent.getContentPane().add(scrollPane);
		
		btnBack = new JButton("Back");
		btnBack.setBounds(856, 639, 129, 44);
		frmNewsagent.getContentPane().add(btnBack);
		
		btnListActiveOrders = new JButton("List active orders");
		btnListActiveOrders.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refresh("select * from orders where order_status!=\"Canceled\"");
			}
		});
		btnListActiveOrders.setBounds(42, 526, 197, 38);
		frmNewsagent.getContentPane().add(btnListActiveOrders);
		
		btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refresh("select * from orders");
			}
		});
		btnRefresh.setBounds(42, 639, 123, 44);
		frmNewsagent.getContentPane().add(btnRefresh);
		
		updateInfoTF = new JTextField();
		updateInfoTF.setBounds(42, 464, 310, 38);
		frmNewsagent.getContentPane().add(updateInfoTF);
		updateInfoTF.setColumns(10);
		
		JButton btnUpdate = new JButton("Update Order Info");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int n = -2;
				if (updateField.equals("")) {
					DialogMessage.showWarningDialog("Please choose a value to update first!");
					return;
				}
				if (updateInfoTF.getText().trim().equals("")) {
					DialogMessage.showWarningDialog("update info is empty!");
					return;
				}
				if (updateField.equals("order_id")||updateField.equals("customer_id")||updateField.equals("item_id")) {
					DialogMessage.showWarningDialog("Can not update a id!");
					return;
				}
				if(updateField.equals("order_date")) {
					DialogMessage.showWarningDialog("Can not update order date!");
					return;
				}
				try {
					n=orderDao.updateField(updateField, orderId, updateInfoTF.getText());
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				if(n>0)
					DialogMessage.showInfoDialog("update successful!");
				refresh("select * from orders");
			}
		});
		btnUpdate.setBounds(399, 464, 183, 38);
		frmNewsagent.getContentPane().add(btnUpdate);
		
		TableofDBContents.addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				Object o = e.getSource();
				if (o instanceof JTable) {
					JTable t = (JTable) o;
					QueryTableModel qtb = (QueryTableModel) t.getModel();
					orderId = (String) qtb.getValueAt(t.getSelectedRow(), 0);
					updateField = (String) qtb.getColumnName(t.getSelectedColumn());
				}
			}
		});
		
		refresh("select * from orders");
		
		// center window
		frmNewsagent.setLocationRelativeTo(null);
	}

	private void refresh(String showSql) {
		try {
			TableModel.refreshFromDB(new DBUtils().getConnection().createStatement(),showSql);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
