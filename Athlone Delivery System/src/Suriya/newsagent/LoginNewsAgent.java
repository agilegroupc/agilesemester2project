package Suriya.newsagent;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;

import Suriya.DB.NewsAgentDAO;
import Suriya.DB.TBNewsAgent;
import Suriya.newsagent.DialogMessage;
import Suriya.newsagent.Verify;

import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LoginNewsAgent extends JFrame{

	private JFrame frmLn;
	private JTextField txtEmailId;
	private JPasswordField txtPassword;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginNewsAgent window = new LoginNewsAgent();
					window.frmLn.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LoginNewsAgent() {
		initialize();
		frmLn.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmLn = new JFrame();
		frmLn.setBounds(100, 100, 450, 300);
		frmLn.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmLn.getContentPane().setLayout(null);
		NewsAgentDAO newDao = new NewsAgentDAO();
		
		JLabel lblLoginPage = new JLabel("Login");
		lblLoginPage.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblLoginPage.setBounds(175, 34, 58, 20);
		frmLn.getContentPane().add(lblLoginPage);
		
		JLabel lblEmailId = new JLabel("Email ID: ");
		lblEmailId.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEmailId.setBounds(71, 79, 83, 20);
		frmLn.getContentPane().add(lblEmailId);
		
		txtEmailId = new JTextField();
		txtEmailId.setBounds(175, 81, 162, 20);
		frmLn.getContentPane().add(txtEmailId);
		txtEmailId.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPassword.setBounds(71, 144, 83, 14);
		frmLn.getContentPane().add(lblPassword);
		
		txtPassword = new JPasswordField();
		txtPassword.setBounds(175, 143, 162, 20);
		frmLn.getContentPane().add(txtPassword);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new newsAgentStartUp();
				frmLn.dispose();
			}
		});
		btnBack.setBounds(71, 205, 89, 23);
		frmLn.getContentPane().add(btnBack);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				String email=txtEmailId.getText();
				String pass=txtPassword.getText();
				if(email.trim().equals("")||pass.trim().equals("")) {
					DialogMessage.showWarningDialog("Empty log in info!!!");
					return;
				}
				if(!Verify.verifyEmail(email)) {
					DialogMessage.showWarningDialog("Invalid email format!!!");
					return;
				}
				if(!Verify.verifyPasswordFormat(pass)) {
					DialogMessage.showWarningDialog("Invalid password format!!!");
					return;
				}
				if(pass.equals(newDao.queryPassword(email)))
				{
						new NewsAgentMainPage();
						frmLn.dispose();
				}
				else if(newDao.queryPassword(email)!=null)
				{
						DialogMessage.showWarningDialog("Wrong password!!!");
						return;
				}
				else 
				{
						DialogMessage.showWarningDialog("Acccount does not exist!!!");
						return;
				}
			} 
		});

		btnLogin.setBounds(175, 205, 89, 23);
		frmLn.getContentPane().add(btnLogin);
		
		//center window
		frmLn.setLocationRelativeTo(null); 
	}

}
