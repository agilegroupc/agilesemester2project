package Suriya.newsagent;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.naming.ldap.Rdn;
import javax.swing.JButton;
import javax.swing.JTable;

import Suriya.DB.QueryTableModel;

import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JRadioButton;

public class DisplayDeliveryPersonnelInfo extends JFrame{

	private JFrame frmDeliveryPersonnel;
	QueryTableModel TableModel = new QueryTableModel();
	private JTable table = new JTable(TableModel);
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DisplayDeliveryPersonnelInfo window = new DisplayDeliveryPersonnelInfo();
					window.frmDeliveryPersonnel.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public DisplayDeliveryPersonnelInfo() {
		initialize();
		frmDeliveryPersonnel.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmDeliveryPersonnel = new JFrame();
		frmDeliveryPersonnel.setTitle("Delivery Personnel ");
		frmDeliveryPersonnel.setBounds(100, 100, 729, 512);
		frmDeliveryPersonnel.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmDeliveryPersonnel.getContentPane().setLayout(null);

		JRadioButton rdBtnAvailable = new JRadioButton("Show Only Available Delivery Personnel");
		
		rdBtnAvailable.setBounds(91, 27, 277, 23);
		frmDeliveryPersonnel.getContentPane().add(rdBtnAvailable);
		
		JRadioButton rdBtnAll = new JRadioButton("Show all Delivery Personnel");
		rdBtnAll.setBounds(370, 27, 212, 23);
		frmDeliveryPersonnel.getContentPane().add(rdBtnAll);
		
		rdBtnAvailable.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				rdBtnAll.setSelected(false);
			}
		});
		
		rdBtnAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				rdBtnAvailable.setSelected(false);
			}
		});
		
		JButton btnShowInfo = new JButton("Show Delivery Person Information");
		btnShowInfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(rdBtnAvailable.isSelected())
				{
					TableModel.callDelivery(2);
					rdBtnAll.setSelected(false);
					
				}
				else if(rdBtnAll.isSelected())
				{
					rdBtnAvailable.setSelected(false);
					TableModel.callDelivery(1);
				}
				else
				{
					DialogMessage.showInfoDialog("Please select an option");
				}
				table.setModel(TableModel);
			}
		});
		btnShowInfo.setBounds(164, 359, 399, 23);
		frmDeliveryPersonnel.getContentPane().add(btnShowInfo);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(45, 67, 636, 281);
		frmDeliveryPersonnel.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JButton btnNewButton = new JButton("Back");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new NewsAgentMainPage();
				frmDeliveryPersonnel.dispose();
			}
		});
		btnNewButton.setBounds(164, 427, 399, 23);
		frmDeliveryPersonnel.getContentPane().add(btnNewButton);
		
		JButton btnAssignDeliveryPersonnel = new JButton("Assign Delivery Area");
		btnAssignDeliveryPersonnel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new assignDeliveryArea();
				frmDeliveryPersonnel.dispose();
			}
		});
		btnAssignDeliveryPersonnel.setBounds(164, 393, 399, 23);
		frmDeliveryPersonnel.getContentPane().add(btnAssignDeliveryPersonnel);
		
	}
}
