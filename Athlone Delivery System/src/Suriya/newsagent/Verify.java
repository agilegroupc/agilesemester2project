package Suriya.newsagent;

public class Verify {
	public static boolean verifyName(String name) {
		String regex="[a-zA-Z]{3,15}";
		return name.matches(regex);
	}
	public static boolean verifyPhone(String phone) {
		// 9 digits phone number
		String regex = "[0-9]{9}";
		return phone.matches(regex);
	}
	
	public static boolean verifyEmail(String email) {
		String regex = "(\\w+)([\\w+.-])*@(\\w+)([\\w+.-])*\\.\\w+";
		return email.matches(regex);
	}
	public static boolean verifyPasswordFormat(String password) {
		String regex="[0-9a-zA-Z]{6,18}";
		return password.matches(regex);
	}
	public static boolean verifyAge(int age) {
		if (age >= 18 && age <= 100) {
			return true;
		}
		return false;
	}
	public static boolean verifyArea(int area)
	{
		if(area<0 || area>15)
		{
			return false;
		}
		return true;
	}
	public static boolean verifyGender(String gender) {
		if(gender.equals("m" ) || gender.equals("f") || gender.equals("M") || gender.equals("F")) {
			return true;
		}
		return false;
	}
	public static boolean verifyAddress(String address) {
		//letters, numbers and spaces 
		String regex="[0-9a-zA-Z\\s+]{10,50}";
		return address.matches(regex);
	}

}
