package Suriya.newsagent;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import billing.CustomerBills;
import orders.OrdersManagementPage;

public class NewsAgentMainPage extends JFrame{

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NewsAgentMainPage window = new NewsAgentMainPage();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public NewsAgentMainPage() {
		initialize();
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 479, 324);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("NewsAgent Main Page");
		lblNewLabel.setFont(new Font("Tw Cen MT Condensed", Font.PLAIN, 16));
		lblNewLabel.setBounds(153, 24, 191, 48);
		frame.getContentPane().add(lblNewLabel);
		
		JButton btnCustomerInfo = new JButton("Customer Information");
		btnCustomerInfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new DisplayCustomerInfo();
				frame.dispose();
			}
		});
		btnCustomerInfo.setBounds(98, 83, 246, 23);
		frame.getContentPane().add(btnCustomerInfo);
		
		JButton btnDeliveryPersonnel = new JButton("Delivery Personnel Information");
		btnDeliveryPersonnel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new DisplayDeliveryPersonnelInfo();
				frame.dispose();
			}
		});
		btnDeliveryPersonnel.setBounds(98, 161, 246, 23);
		frame.getContentPane().add(btnDeliveryPersonnel);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				new newsAgentStartUp();
			}
		});
		btnBack.setBounds(98, 237, 246, 27);
		frame.getContentPane().add(btnBack);
		
		JButton btnOrdersInformation = new JButton("Orders Information");
		btnOrdersInformation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new OrdersManagementPage();
				frame.dispose();
			}
		});
		btnOrdersInformation.setBounds(98, 121, 246, 27);
		frame.getContentPane().add(btnOrdersInformation);
		
		JButton btnBillingInformation = new JButton("Billing Information");
		btnBillingInformation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			new NewsagentBillingPage();
			frame.dispose();
			}
		});
		btnBillingInformation.setBounds(98, 197, 246, 27);
		frame.getContentPane().add(btnBillingInformation);
		
		frame.setLocationRelativeTo(null);
	}
}
