package Suriya.newsagent;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;

import Suriya.DB.NewsAgentDAO;
import Suriya.DB.TBNewsAgent;
import Suriya.newsagent.DialogMessage;
import Suriya.newsagent.Verify;

import javax.swing.JPasswordField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class RegisterNewsAgent extends JFrame{

	private JFrame frmRn;
	private JTextField txtManagersName;
	private JTextField txtContactNumber;
	private JTextField txtEmailId;
	private JPasswordField txtPassword;
	private NewsAgentDAO newDao=new NewsAgentDAO();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegisterNewsAgent window = new RegisterNewsAgent();
					window.frmRn.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public RegisterNewsAgent() {
		initialize();
		frmRn.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmRn = new JFrame();
		frmRn.setBounds(100, 100, 572, 396);
		frmRn.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmRn.getContentPane().setLayout(null);
		
		JLabel lblRegister = new JLabel("Register As NewsAgent");
		lblRegister.setBounds(166, 31, 262, 28);
		lblRegister.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
		frmRn.getContentPane().add(lblRegister);
		
		JLabel lblManagersName = new JLabel("Manager's Name");
		lblManagersName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblManagersName.setBounds(49, 91, 117, 22);
		frmRn.getContentPane().add(lblManagersName);
		
		txtManagersName = new JTextField();
		txtManagersName.setBounds(183, 94, 189, 20);
		frmRn.getContentPane().add(txtManagersName);
		txtManagersName.setColumns(10);
		
		JLabel lbContactNumber = new JLabel("Contact Number");
		lbContactNumber.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lbContactNumber.setBounds(49, 154, 117, 14);
		frmRn.getContentPane().add(lbContactNumber);
		
		txtContactNumber = new JTextField();
		txtContactNumber.setBounds(183, 153, 186, 20);
		frmRn.getContentPane().add(txtContactNumber);
		txtContactNumber.setColumns(10);
		
		JLabel lblEmailID = new JLabel("Email Id");
		lblEmailID.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEmailID.setBounds(49, 207, 88, 14);
		frmRn.getContentPane().add(lblEmailID);
		
		txtEmailId = new JTextField();
		txtEmailId.setBounds(183, 206, 189, 20);
		frmRn.getContentPane().add(txtEmailId);
		txtEmailId.setColumns(10);
		
		JLabel lblPassword = new JLabel("Passwword");
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPassword.setBounds(49, 256, 100, 14);
		frmRn.getContentPane().add(lblPassword);
		
		txtPassword = new JPasswordField();
		txtPassword.setBounds(183, 255, 189, 20);
		frmRn.getContentPane().add(txtPassword);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new newsAgentStartUp();
				frmRn.dispose();
			}
		});
		btnBack.setBounds(113, 308, 89, 23);
		frmRn.getContentPane().add(btnBack);
		
		JButton btnRegister = new JButton("Register");
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(txtManagersName.getText().trim().equals("") || txtContactNumber.getText().trim().equals("") || txtEmailId.getText().trim().equals("")
						|| txtPassword.getText().trim().equals(""))
				{
					DialogMessage.showWarningDialog("Empty register info!");
					return;
				}
				String managersName = txtManagersName.getText().trim();
				String contactNumber = txtContactNumber.getText().trim();
				String emailId = txtEmailId.getText().trim();
				String password = txtPassword.getText().trim();
				int count = 0;
				if(!Verify.verifyName(managersName))
				{
					DialogMessage.showWarningDialog("Invalid Name format!");
					count ++;
					return;
				}
				if(!Verify.verifyPhone(contactNumber))
				{
					DialogMessage.showWarningDialog("Invalid Contact Number!");
					count ++;
					return;
				}
				if(!Verify.verifyEmail(emailId))
				{
					DialogMessage.showWarningDialog("Invalid Email Id!");
					count ++;
					return;
				}
				if(!Verify.verifyPasswordFormat(password))
				{
					DialogMessage.showWarningDialog("Invalid Password format!");
					count ++;
					return;
				}
				
				TBNewsAgent newsagent=new TBNewsAgent();
				newsagent.setManagerName(managersName);
				newsagent.setPhone(contactNumber);
				newsagent.setEmail(emailId);
				newsagent.setPassword(password);
				int num=newDao.registerInsert(newsagent);
				if (num > 0) {
					DialogMessage.showInfoDialog("Register successfully!!!");
					new NewsAgentMainPage();
					frmRn.dispose();
				}
			}
		});
		btnRegister.setBounds(232, 308, 89, 23);
		frmRn.getContentPane().add(btnRegister);
	}
}
