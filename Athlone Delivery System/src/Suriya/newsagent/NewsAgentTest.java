package Suriya.newsagent;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Suriya.newsagent.Verify;
public class NewsAgentTest {

	@Before
	public void setUp() throws Exception {
	}
	
	@Test
	public void testVerifyName() {
		assertEquals(true, Verify.verifyName("Suriya"));
	}
	@Test
	public void testVerifyPhone() {
		assertEquals(true, Verify.verifyPhone("123456789"));
	}

	@Test
	public void testVerifyEmail() {
		assertEquals(false, Verify.verifyEmail("ssdads"));
	}

	@Test
	public void testVerifyPasswordFormat() {
		assertEquals(true, Verify.verifyPasswordFormat("123456"));
	}
	@Test
	public void testVerifyAge() {
		assertEquals(true, Verify.verifyAge(18));
	}

	@Test
	public void testVerifyGender() {
		assertEquals(false, Verify.verifyGender("ssdads"));
	}

	@Test
	public void testVerifyAddress() {
		assertEquals(true, Verify.verifyAddress("Wellmount student village"));
	}
}
