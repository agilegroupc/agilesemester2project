package Suriya.newsagent;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import Suriya.DB.NewsAgentDAO;
import Suriya.DB.QueryTableModel;
import Suriya.DB.TBCustomer;

public class updateCustomerInfo extends JFrame{

	private JFrame frmUpdateCustomerInformation;
	private JTextField txtId;
	private JTextField txtFname;
	private JTextField txtLname;
	private JTextField txtAge;
	private JTextField txtGender;
	private JTextField txtAddress;
	private JTextField txtPhoneNo;
	private JTextField txtEmail;
	QueryTableModel TableModel = new QueryTableModel();
	private JTable table = new JTable(TableModel);
	NewsAgentDAO newDao = new NewsAgentDAO();
	TBCustomer newCus = new TBCustomer();
	int CustomerID ;
	String CustomerFname;
	String CustomerLname ;
	String CustomerAge;
	String CustomerGender;
	String CustomerAddress ;
	String CustomerPhoneNo ;
	String CustomerEmail ;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					updateCustomerInfo window = new updateCustomerInfo();
					window.frmUpdateCustomerInformation.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public updateCustomerInfo() {
		initialize();
		frmUpdateCustomerInformation.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmUpdateCustomerInformation = new JFrame();
		frmUpdateCustomerInformation.setTitle("Update Customer Information");
		frmUpdateCustomerInformation.setBounds(100, 100, 857, 485);
		frmUpdateCustomerInformation.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmUpdateCustomerInformation.getContentPane().setLayout(null);
		
		JLabel lblId = new JLabel("Enter Customer Id:");
		lblId.setBounds(32, 48, 114, 14);
		frmUpdateCustomerInformation.getContentPane().add(lblId);
		
		txtId = new JTextField();
		txtId.setBounds(156, 45, 140, 20);
		frmUpdateCustomerInformation.getContentPane().add(txtId);
		txtId.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("First Name:");
		lblNewLabel.setBounds(32, 95, 92, 14);
		frmUpdateCustomerInformation.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Last Name:");
		lblNewLabel_1.setBounds(32, 137, 92, 14);
		frmUpdateCustomerInformation.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Age:");
		lblNewLabel_2.setBounds(32, 180, 46, 14);
		frmUpdateCustomerInformation.getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Gender:");
		lblNewLabel_3.setBounds(32, 221, 46, 14);
		frmUpdateCustomerInformation.getContentPane().add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Address:");
		lblNewLabel_4.setBounds(32, 267, 46, 14);
		frmUpdateCustomerInformation.getContentPane().add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Phone No:");
		lblNewLabel_5.setBounds(32, 308, 92, 14);
		frmUpdateCustomerInformation.getContentPane().add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("Email:");
		lblNewLabel_6.setBounds(32, 349, 46, 14);
		frmUpdateCustomerInformation.getContentPane().add(lblNewLabel_6);
		
		txtFname = new JTextField();
		txtFname.setBounds(156, 92, 140, 20);
		frmUpdateCustomerInformation.getContentPane().add(txtFname);
		txtFname.setColumns(10);
		
		txtLname = new JTextField();
		txtLname.setBounds(156, 134, 140, 20);
		frmUpdateCustomerInformation.getContentPane().add(txtLname);
		txtLname.setColumns(10);
		
		txtAge = new JTextField();
		txtAge.setBounds(156, 177, 140, 20);
		frmUpdateCustomerInformation.getContentPane().add(txtAge);
		txtAge.setColumns(10);
		
		txtGender = new JTextField();
		txtGender.setBounds(156, 218, 140, 20);
		frmUpdateCustomerInformation.getContentPane().add(txtGender);
		txtGender.setColumns(10);
		
		txtAddress = new JTextField();
		txtAddress.setBounds(156, 264, 140, 20);
		frmUpdateCustomerInformation.getContentPane().add(txtAddress);
		txtAddress.setColumns(10);
		
		txtPhoneNo = new JTextField();
		txtPhoneNo.setBounds(156, 305, 140, 20);
		frmUpdateCustomerInformation.getContentPane().add(txtPhoneNo);
		txtPhoneNo.setColumns(10);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(156, 346, 140, 20);
		frmUpdateCustomerInformation.getContentPane().add(txtEmail);
		txtEmail.setColumns(10);
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(txtId.getText().toString().trim().equals("") || txtFname.getText().toString().trim().equals("") || txtLname.getText().toString().trim().equals("")
						|| txtAge.getText().toString().trim().equals("") || txtGender.getText().toString().trim().equals("")
						|| txtAddress.getText().toString().trim().equals("") || txtPhoneNo.getText().toString().trim().equals("")
						|| txtEmail.getText().toString().trim().equals("")
						)
				{
					DialogMessage.showInfoDialog("One or more field is empty!");
					return;
				}
				
				CustomerID = Integer.parseInt(txtId.getText().toString().trim());
				CustomerFname = txtFname.getText().toString().trim();
				 CustomerLname = txtLname.getText().toString().trim();
				CustomerAge = txtAge.getText().toString().trim();
				CustomerGender = txtGender.getText().toString().trim();
				 CustomerAddress = txtAddress.getText().toString().trim();
				 CustomerPhoneNo = txtPhoneNo.getText().toString().trim();
				 CustomerEmail = txtEmail.getText().toString().trim();
				if(!Verify.verifyName(CustomerFname))
				{
					DialogMessage.showWarningDialog("Invalid Name format!");
					return;
				}
				if(!Verify.verifyName(CustomerLname))
				{
					DialogMessage.showWarningDialog("Invalid Name format!");
					return;
				}
				if(!Verify.verifyAge(Integer.parseInt(CustomerAge)))
				{
					DialogMessage.showWarningDialog("Invalid Age format!");
					return;
				}
				if(!Verify.verifyGender(CustomerGender))
				{
					DialogMessage.showWarningDialog("Invalid Gender format!");
					return;
				}
				/*if(!Verify.verifyAddress(CustomerAddress))
				{
					DialogMessage.showWarningDialog("Invalid Address format!");
					return;
				}*/
				if(!Verify.verifyPhone(CustomerPhoneNo))
				{
					DialogMessage.showWarningDialog("Invalid Phone Number Format!");
					return;
				}
				if(!Verify.verifyEmail(CustomerEmail))
				{
					DialogMessage.showWarningDialog("Invalid Email Id!");
					return;
				}
				
				
				newCus.setAddress(CustomerAddress);
				newCus.setCustomerId(CustomerID);
				newCus.setAge(Integer.parseInt(CustomerAge));
				newCus.setEmail(CustomerEmail);
				newCus.setPhone(CustomerPhoneNo);
				newCus.setGender(CustomerGender);
				newCus.setFirstName(CustomerFname);
				newCus.setLastName(CustomerLname);
				int num = 0;
				num = newDao.updateCustomer(newCus, CustomerID);
				if(num>0)
				{
					DialogMessage.showInfoDialog("Successful");
					TableModel.callCustomer(1);table.setModel(TableModel);
				}
			}
		});
		btnUpdate.setBounds(32, 412, 264, 23);
		frmUpdateCustomerInformation.getContentPane().add(btnUpdate);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(356, 48, 448, 364);
		frmUpdateCustomerInformation.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		TableModel.callCustomer(1);table.setModel(TableModel);
JButton btnBack = new JButton("Back");
btnBack.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent e) {
		new DisplayCustomerInfo();
		frmUpdateCustomerInformation.dispose();
	}
});
btnBack.setBounds(32, 378, 264, 23);
frmUpdateCustomerInformation.getContentPane().add(btnBack);
	}
}
