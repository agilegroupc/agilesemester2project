package Suriya.newsagent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;


public class updateCustomerInfoTest {

	@Test
	public void testVerifyName() {
		assertEquals(true, Verify.verifyName("Suriya"));
	}
	@Test
	public void testVerifyPhone() {
		assertEquals(true, Verify.verifyPhone("123456789"));
	}

	@Test
	public void testVerifyEmail() {
		assertEquals(true, Verify.verifyEmail("abc@gmail.com"));
	}
	@Test
	public void testVerifyAge() {
		assertEquals(true, Verify.verifyAge(18));
	}

	@Test
	public void testVerifyGender() {
		assertNotEquals(false, Verify.verifyGender("m"));
	}
	

	@Test
	public void testVerifyAddress() {
		assertEquals(true, Verify.verifyAddress("Wellmount student village"));
	}
}
