package Suriya.newsagent;

import java.awt.EventQueue;

import javax.swing.JFrame;

import StartupGUI.TempSartup;

import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class newsAgentStartUp extends JFrame{

	private JFrame frmNewsagentLogIn;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					newsAgentStartUp window = new newsAgentStartUp();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public newsAgentStartUp() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmNewsagentLogIn = new JFrame();
		frmNewsagentLogIn.setTitle("Newsagent Log In Page");
		frmNewsagentLogIn.setBounds(100, 100, 492, 344);
		frmNewsagentLogIn.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmNewsagentLogIn.getContentPane().setLayout(null);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new LoginNewsAgent();
				frmNewsagentLogIn.dispose();
			}
		});
		btnLogin.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		btnLogin.setBounds(134, 76, 158, 39);
		frmNewsagentLogIn.getContentPane().add(btnLogin);
		
		JButton btnRegister = new JButton("Register");
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new RegisterNewsAgent();
				frmNewsagentLogIn.dispose();
			}
		});
		btnRegister.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		btnRegister.setBounds(134, 159, 158, 39);
		frmNewsagentLogIn.getContentPane().add(btnRegister);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new TempSartup();
				frmNewsagentLogIn.dispose();
			}
		});
		btnBack.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnBack.setBounds(134, 230, 158, 39);
		frmNewsagentLogIn.getContentPane().add(btnBack);
		
		frmNewsagentLogIn.setLocationRelativeTo(null);
		
		frmNewsagentLogIn.setVisible(true);
	}
}
