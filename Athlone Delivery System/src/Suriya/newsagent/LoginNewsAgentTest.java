package Suriya.newsagent;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class LoginNewsAgentTest {

	@Before
	public void setUp() throws Exception {
	}
	@Test
	public void testVerifyName() {
		assertEquals(true, Verify.verifyName("Suriya"));
	}
	@Test
	public void testVerifyEmail() {
		assertEquals(false, Verify.verifyEmail("ssdas"));
	}

	@Test
	public void testVerifyPasswordFormat() {
		assertEquals(true, Verify.verifyPasswordFormat("123456"));
	}

}
