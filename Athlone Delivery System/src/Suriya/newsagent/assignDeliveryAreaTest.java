package Suriya.newsagent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Before;
import org.junit.Test;

import Suriya.DB.DBUtils;
import Suriya.DB.NewsAgentDAO;
import Suriya.DB.TBNewsAgent;

public class assignDeliveryAreaTest {

	@Before
	public void setUp() throws Exception {
	}
	@Test
	public void testGetConnection() throws Exception{
		DBUtils dbc;
		
			dbc = DBUtils.getDBConnection();
			assertNotEquals(null, dbc.getConnection());
		
	}
	//Test to check so that null values are not update
	@Test
	public void testAssignArea()
	{
		NewsAgentDAO newDao = new NewsAgentDAO();
		TBNewsAgent agent = new TBNewsAgent();
		agent.setArea(1);
		newDao.setConnection("com.mysql.cj.jdbc.Driver",
				"jdbc:mysql://localhost:3306/athlone_delivery_system?serverTimezone=UTC", 
				"root", "password");
		assertEquals(0, newDao.updateArea(agent, 1));
	}
	@Test
	//Test written to succeed 
	public void testAssignAreaF()
	{
		NewsAgentDAO newDao = new NewsAgentDAO();
		//TBNewsAgent agent = new TBNewsAgent();
		//agent.setArea(1);
		newDao.setConnection("com.mysql.cj.jdbc.Driver",
				"jdbc:mysql://localhost:3306/athlone_delivery_system?serverTimezone=UTC", 
				"root", "password");
		assertEquals(1, newDao.updateAreaTest(1, 1));
	}
	@Test 
	public void verifyAreaTest1()
	{
		assertNotEquals(false, Verify.verifyArea(0));
	}
	@Test 
	public void verifyAreaTest2()
	{
		assertNotEquals(0, Verify.verifyArea(16));
	}
	@Test 
	public void verifyAreaTest3()
	{
		assertEquals(true, Verify.verifyArea(5));
	}
	
	
	

}
