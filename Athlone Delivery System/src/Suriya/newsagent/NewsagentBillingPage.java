package Suriya.newsagent;

import java.awt.EventQueue;

import javax.swing.JFrame;

import billing.BillsManagement;
import billing.CustomerBills;
import billing.TranferBillsGUI;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class NewsagentBillingPage {

	private JFrame frmNewsagentBillingPage;

	private JButton btnTransferBill = new JButton("Transfer Bill");
	private JButton btnBillManagement = new JButton("Bill Management");
	private JButton btnCustomerBills = new JButton("Customer Bills");
	private JButton btnBack = new JButton("Back");

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NewsagentBillingPage window = new NewsagentBillingPage();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public NewsagentBillingPage() {
		initialize();
		frmNewsagentBillingPage.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmNewsagentBillingPage = new JFrame();
		frmNewsagentBillingPage.setTitle("Newsagent Billing Page");
		frmNewsagentBillingPage.setBounds(100, 100, 454, 380);
		frmNewsagentBillingPage.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frmNewsagentBillingPage.setLocationRelativeTo(null);
		frmNewsagentBillingPage.getContentPane().setLayout(null);
		btnTransferBill.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new TranferBillsGUI();
				frmNewsagentBillingPage.dispose();
			}
		});
		
		btnTransferBill.setBounds(91, 27, 214, 27);
		frmNewsagentBillingPage.getContentPane().add(btnTransferBill);
		
		btnBillManagement.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new BillsManagement();
				frmNewsagentBillingPage.dispose();
			}
		});
		btnBillManagement.setBounds(91, 150, 214, 27);
		frmNewsagentBillingPage.getContentPane().add(btnBillManagement);
		btnCustomerBills.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new CustomerBills();
				frmNewsagentBillingPage.dispose();
			}
		});
		
		btnCustomerBills.setBounds(91, 88, 214, 27);
		frmNewsagentBillingPage.getContentPane().add(btnCustomerBills);
		
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmNewsagentBillingPage.dispose();
				new NewsAgentMainPage();
			}
		});
		btnBack.setBounds(91, 208, 214, 27);
		frmNewsagentBillingPage.getContentPane().add(btnBack);
	}
}
