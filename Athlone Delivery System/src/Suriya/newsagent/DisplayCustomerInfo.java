package Suriya.newsagent;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTable;

import Suriya.DB.NewsAgentDAO;
import Suriya.DB.QueryTableModel;

import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.JRadioButton;

public class DisplayCustomerInfo extends JFrame{

	private JFrame frame;
	QueryTableModel TableModel = new QueryTableModel();
	private JTable table = new JTable(TableModel);
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DisplayCustomerInfo window = new DisplayCustomerInfo();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public DisplayCustomerInfo() {
		initialize();
		frame.setVisible(true);

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 636, 490);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JRadioButton rdBtnAll = new JRadioButton("Show all Customer Info");
		rdBtnAll.setBounds(114, 17, 175, 23);
		frame.getContentPane().add(rdBtnAll);
		
		JRadioButton rdBtnSuspended = new JRadioButton("Show Customers With Suspended Deliveries");
		rdBtnSuspended.setBounds(317, 17, 286, 23);
		frame.getContentPane().add(rdBtnSuspended);
		
		JRadioButton rdbtnShowUnpaid = new JRadioButton("Show Customers With Unpaid Deliveries");
		rdbtnShowUnpaid.setBounds(185, 43, 341, 23);
		frame.getContentPane().add(rdbtnShowUnpaid);
		rdBtnAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				rdBtnSuspended.setSelected(false);
				rdbtnShowUnpaid.setSelected(false);
			}
		});
		rdBtnSuspended.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				rdbtnShowUnpaid.setSelected(false);
				rdBtnAll.setSelected(false);
			}
		});
		rdbtnShowUnpaid.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				rdBtnSuspended.setSelected(false);
				rdBtnAll.setSelected(false);;
			}
		});
		JButton btnDisplayCustomerInfo = new JButton("Display Customer Info");
		btnDisplayCustomerInfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(rdBtnSuspended.isSelected())
				{
					TableModel.callCustomer(2);
					rdBtnAll.setSelected(false);
					
				}
				else if(rdBtnAll.isSelected())
				{
					rdBtnSuspended.setSelected(false);
					TableModel.callCustomer(1);
				}
				else if(rdbtnShowUnpaid.isSelected())
				{
					rdBtnSuspended.setSelected(false);
					TableModel.callCustomer(3);
				}
				else
				{
					DialogMessage.showInfoDialog("Please select an option");
				}
				table.setModel(TableModel);
			}
		});
		btnDisplayCustomerInfo.setBounds(99, 340, 427, 23);
		frame.getContentPane().add(btnDisplayCustomerInfo);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(40, 84, 527, 245);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JButton btnNewButton = new JButton("Back");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new NewsAgentMainPage();
				frame.dispose();
			}
		});
		btnNewButton.setBounds(99, 406, 427, 23);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnUpdate = new JButton("Update Customer Information");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new updateCustomerInfo();frame.dispose();
			}
		});
		btnUpdate.setBounds(99, 374, 427, 23);
		frame.getContentPane().add(btnUpdate);
		
		
		
		
	}

}
