package Suriya.newsagent;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import Suriya.DB.NewsAgentDAO;
import Suriya.DB.QueryTableModel;
import Suriya.DB.TBNewsAgent;

public class assignDeliveryArea extends JFrame{

	private JFrame frmAssignDeliveryArea;
	private JTextField txtId;
	private JTextField txtInputArea;
	
	TBNewsAgent newAgent = new TBNewsAgent();
	NewsAgentDAO newDao = new NewsAgentDAO();
	QueryTableModel TableModel = new QueryTableModel();
	private JTable table = new JTable(TableModel);
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					assignDeliveryArea window = new assignDeliveryArea();
					window.frmAssignDeliveryArea.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public assignDeliveryArea() {
		initialize();
		frmAssignDeliveryArea.setVisible(true);TableModel.callDelivery(1);

		table.setModel(TableModel);
		
		JButton btnPrintDeliveryNote = new JButton("Print Delivery Note");
		btnPrintDeliveryNote.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int DpId;
				try {
					DpId = Integer.parseInt(txtId.getText());
				     System.out.println("An integer");
				}
				catch (NumberFormatException ex) {
				     //Not an integer
					DialogMessage.showWarningDialog("Characters are not allowed!");
					return;
				}
				int num = newDao.printDeliveryNote(newAgent, DpId);
				if(num>0)
				{
					DialogMessage.showInfoDialog("Successful, your file is ready!");	
				}
			}
		});
		btnPrintDeliveryNote.setBounds(21, 324, 474, 23);
		frmAssignDeliveryArea.getContentPane().add(btnPrintDeliveryNote);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAssignDeliveryArea = new JFrame();
		frmAssignDeliveryArea.setTitle("Assign Delivery Area");
		frmAssignDeliveryArea.setBounds(100, 100, 538, 431);
		frmAssignDeliveryArea.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAssignDeliveryArea.getContentPane().setLayout(null);
		

		JLabel lblNewLabel = new JLabel("Enter Delivery Personnel ID");
		lblNewLabel.setBounds(21, 43, 158, 14);
		frmAssignDeliveryArea.getContentPane().add(lblNewLabel);
		
		txtId = new JTextField();
		txtId.setBounds(189, 40, 306, 20);
		frmAssignDeliveryArea.getContentPane().add(txtId);
		txtId.setColumns(10);
		
		JLabel txtArea = new JLabel("Enter Delivery Area");
		txtArea.setBounds(21, 89, 125, 14);
		frmAssignDeliveryArea.getContentPane().add(txtArea);
		
		txtInputArea = new JTextField();
		txtInputArea.setBounds(189, 86, 306, 20);
		frmAssignDeliveryArea.getContentPane().add(txtInputArea);
		txtInputArea.setColumns(10);
		
		JButton btnAssign = new JButton("Assign Delivery Area");
		btnAssign.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int num=0;
				if(txtId.getText().toString().trim().equals("") || txtArea.getText().toString().trim().equals(""))
				{
					DialogMessage.showInfoDialog("One or more field is empty!");
					return;
				}
				int DpId;
				try {
					DpId = Integer.parseInt(txtId.getText());
				     System.out.println("An integer");
				}
				catch (NumberFormatException ex) {
				     //Not an integer
					DialogMessage.showWarningDialog("Characters are not allowed!");
					return;
				}
				
				int area = Integer.parseInt(txtInputArea.getText());
				
				if(!Verify.verifyArea(area))
				{
					DialogMessage.showWarningDialog("Invalid Area code!");
					return;
				}
				newAgent.setArea(area);
				newAgent.setDpID(DpId);
				num=newDao.updateArea(newAgent,area);
				if(num>0)
				{
					DialogMessage.showInfoDialog("Successful");
					TableModel.callDelivery(1);
					table.setModel(TableModel);

				}
			}
		});
		btnAssign.setBounds(21, 291, 474, 23);
		frmAssignDeliveryArea.getContentPane().add(btnAssign);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new NewsAgentMainPage();
				frmAssignDeliveryArea.dispose();
			}
		});
		btnBack.setBounds(21, 358, 474, 23);
		frmAssignDeliveryArea.getContentPane().add(btnBack);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(21, 125, 474, 156);
		frmAssignDeliveryArea.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
	}
}
