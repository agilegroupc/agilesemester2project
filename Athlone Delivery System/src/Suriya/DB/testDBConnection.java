package Suriya.DB;

import static org.junit.Assert.*;

import org.junit.Test;

import Suriya.DB.DBUtils;
public class testDBConnection {

	@Test
	public void testGetConnection() {
		DBUtils dbc;
		try {
			dbc = DBUtils.getDBConnection();
			assertNotEquals(null, dbc.getConnection());
		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception happened");
		}
	}

}