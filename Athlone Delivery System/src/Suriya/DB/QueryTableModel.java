package Suriya.DB;
import javax.swing.*;
import javax.swing.table.*;

import Suriya.newsagent.DialogMessage;

import java.awt.event.*;
import java.awt.*;
import java.sql.*;
import java.io.*;
import java.util.*;

@SuppressWarnings("serial")
public
class QueryTableModel extends AbstractTableModel
{
	Vector modelData; //will hold String[] objects
	int colCount;
	String[] headers=new String[0] ;
	Connection con;
	Statement stmt = null;
	String[] record;
	ResultSet rs = null;

	public QueryTableModel(){
		modelData = new Vector();
	}//end constructor QueryTableModel

	public String getColumnName(int i){
		return headers[i];
	}	
	public int getColumnCount(){
		return colCount;
	}
	
	public int getRowCount(){
		return modelData.size();
	}
	
	public Object getValueAt(int row, int col){
		return ((String[])modelData.elementAt(row))[col];
	}
	public void initiate_db_conn()
	{
		try
		{
			// Load the JConnector Driver
			Class.forName("com.mysql.jdbc.Driver");
			// Specify the DB Name
			String url="jdbc:mysql://localhost:3306/athlone_delivery_system?serverTimezone=UTC";
			// Connect to DB using DB URL, Username and password
			con = DriverManager.getConnection(url, "root", "mike");
			//Create a generic statement which is passed to the TestInternalFrame1
			stmt = con.createStatement();
			
		}
		catch(Exception e)
		{
			System.out.println("Error: Failed to connect to database\n"+e.getMessage());
		}
	}
	public void callCustomer(int in)
	{
		initiate_db_conn();
		int inn=in;
		
		if(inn==1)
		{
			showCustomerInfo(stmt);
		}
		else if(inn==2)
		{
			showSuspendedshowCustomerInfo(stmt);
		}
		else if(inn==3)
		{
			showCustomerUnpaid(stmt);
		}
		else
		{
			DialogMessage.showInfoDialog("Please Select an option");
		}
	}
	public void callDelivery(int in)
	{
		initiate_db_conn();
		int inn=in;
		
		if(inn==1)
		{
			showDeliveryPersonnelInfo(stmt);
		}
		else if(inn==2)
		{
			showAvailableDeliveryPersonnelInfo(stmt);
		}
		else
		{
			DialogMessage.showInfoDialog("Please Select an option");
		}
	}
	public void showCustomerInfo( Statement stmt1)
	{
		//modelData is the data stored by the table
		//when set query is called the data from the 
		//DB is queried using 揝ELECT * FROM myInfo�? 
		//and the data from the result set is copied 
		//into the modelData. Every time refreshFromDB is
		//called the DB is queried and a new 
		//modelData is created  

		modelData = new Vector();
		stmt = stmt1;
		try{
			//Execute the query and store the result set and its metadata
			rs = stmt.executeQuery("SELECT * FROM customers");
			ResultSetMetaData meta = rs.getMetaData();
		
			//to get the number of columns
			colCount = meta.getColumnCount(); 
			// Now must rebuild the headers array with the new column names
			headers = new String[colCount];
	
			for(int h = 0; h<colCount; h++)
			{
				headers[h] = meta.getColumnName(h+1);
			}//end for loop
		
			// fill the cache with the records from the query, ie get all the rows
		
			while(rs.next())
			{
				record = new String[colCount];
				for(int i = 0; i < colCount; i++)
				{
					record[i] = rs.getString(i+1);
				}//end for loop
				modelData.addElement(record);
			}// end while loop
			fireTableChanged(null); //Tell the listeners a new table has arrived.
		}//end try clause
		catch(Exception e) {
					e.printStackTrace();
		} // end catch clause to query table
	}//end refreshFromDB method
	public void showCustomerUnpaid( Statement stmt1)
	{
		//modelData is the data stored by the table
		//when set query is called the data from the 
		//DB is queried using 揝ELECT * FROM myInfo�? 
		//and the data from the result set is copied 
		//into the modelData. Every time refreshFromDB is
		//called the DB is queried and a new 
		//modelData is created  

		modelData = new Vector();
		stmt = stmt1;
		try{
			//Execute the query and store the result set and its metadata
			rs = stmt.executeQuery("SELECT * FROM customers where billing_status=2");
			ResultSetMetaData meta = rs.getMetaData();
		
			//to get the number of columns
			colCount = meta.getColumnCount(); 
			// Now must rebuild the headers array with the new column names
			headers = new String[colCount];
	
			for(int h = 0; h<colCount; h++)
			{
				headers[h] = meta.getColumnName(h+1);
			}//end for loop
		
			// fill the cache with the records from the query, ie get all the rows
		
			while(rs.next())
			{
				record = new String[colCount];
				for(int i = 0; i < colCount; i++)
				{
					record[i] = rs.getString(i+1);
				}//end for loop
				modelData.addElement(record);
			}// end while loop
			fireTableChanged(null); //Tell the listeners a new table has arrived.
		}//end try clause
		catch(Exception e) {
					e.printStackTrace();
		} // end catch clause to query table
	}//end refreshFromDB method
	public void showSuspendedshowCustomerInfo( Statement stmt1)
	{
		//modelData is the data stored by the table
		//when set query is called the data from the 
		//DB is queried using 揝ELECT * FROM myInfo�? 
		//and the data from the result set is copied 
		//into the modelData. Every time refreshFromDB is
		//called the DB is queried and a new 
		//modelData is created  

		modelData = new Vector();
		stmt = stmt1;
		try{
			//Execute the query and store the result set and its metadata
			rs = stmt.executeQuery("SELECT * FROM customers where available_status=2");
			ResultSetMetaData meta = rs.getMetaData();
		
			//to get the number of columns
			colCount = meta.getColumnCount(); 
			// Now must rebuild the headers array with the new column names
			headers = new String[colCount];
	
			for(int h = 0; h<colCount; h++)
			{
				headers[h] = meta.getColumnName(h+1);
			}//end for loop
		
			// fill the cache with the records from the query, ie get all the rows
		
			while(rs.next())
			{
				record = new String[colCount];
				for(int i = 0; i < colCount; i++)
				{
					record[i] = rs.getString(i+1);
				}//end for loop
				modelData.addElement(record);
			}// end while loop
			fireTableChanged(null); //Tell the listeners a new table has arrived.
		}//end try clause
		catch(Exception e) {
					e.printStackTrace();
		} // end catch clause to query table
	}//end refreshFromDB method
	public void showDeliveryPersonnelInfo( Statement stmt1)
	{
		//modelData is the data stored by the table
		//when set query is called the data from the 
		//DB is queried using 揝ELECT * FROM myInfo�? 
		//and the data from the result set is copied 
		//into the modelData. Every time refreshFromDB is
		//called the DB is queried and a new 
		//modelData is created  

		modelData = new Vector();
		stmt = stmt1;
		try{
			//Execute the query and store the result set and its metadata
			rs = stmt.executeQuery("SELECT * FROM deliveryperson");
			ResultSetMetaData meta = rs.getMetaData();
		
			//to get the number of columns
			colCount = meta.getColumnCount(); 
			// Now must rebuild the headers array with the new column names
			headers = new String[colCount];
	
			for(int h = 0; h<colCount; h++)
			{
				headers[h] = meta.getColumnName(h+1);
			}//end for loop
		
			// fill the cache with the records from the query, ie get all the rows
		
			while(rs.next())
			{
				record = new String[colCount];
				for(int i = 0; i < colCount; i++)
				{
					record[i] = rs.getString(i+1);
				}//end for loop
				modelData.addElement(record);
			}// end while loop
			fireTableChanged(null); //Tell the listeners a new table has arrived.
		}//end try clause
		catch(Exception e) {
					e.printStackTrace();
		} // end catch clause to query table
	}//end refreshFromDB method
	
	public void showAvailableDeliveryPersonnelInfo( Statement stmt1)
	{
		//modelData is the data stored by the table
		//when set query is called the data from the 
		//DB is queried using 揝ELECT * FROM myInfo�? 
		//and the data from the result set is copied 
		//into the modelData. Every time refreshFromDB is
		//called the DB is queried and a new 
		//modelData is created  

		modelData = new Vector();
		stmt = stmt1;
		try{
			//Execute the query and store the result set and its metadata
			rs = stmt.executeQuery("SELECT * FROM deliveryperson where deliveryperson_status='01'");
			ResultSetMetaData meta = rs.getMetaData();
		
			//to get the number of columns
			colCount = meta.getColumnCount(); 
			// Now must rebuild the headers array with the new column names
			headers = new String[colCount];
	
			for(int h = 0; h<colCount; h++)
			{
				headers[h] = meta.getColumnName(h+1);
			}//end for loop
		
			// fill the cache with the records from the query, ie get all the rows
		
			while(rs.next())
			{
				record = new String[colCount];
				for(int i = 0; i < colCount; i++)
				{
					record[i] = rs.getString(i+1);
				}//end for loop
				modelData.addElement(record);
			}// end while loop
			fireTableChanged(null); //Tell the listeners a new table has arrived.
		}//end try clause
		catch(Exception e) {
					e.printStackTrace();
		} // end catch clause to query table
	}//end refreshFromDB method
}// end class QueryTableModel


