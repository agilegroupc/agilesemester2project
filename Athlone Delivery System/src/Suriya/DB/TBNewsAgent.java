package Suriya.DB;


public class TBNewsAgent {
	private String managerName;
	private String phone;
	private String email; 
	private String password;
	private int id;
	private int area;
	private int cusId;
	private int status;
	public String getManagerName() {
		return managerName;
	}
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setDpID(int id)
	{
		this.id=id;
	}
	public int getDpID()
	{
		return id;
	}
	public void setArea(int area)
	{
		this.area=area;
	}
	public int getArea()
	{
		return area;
	}
	public void setcusId(int cusId)
	{
		this.cusId=cusId;
	}
	public int getcusId()
	{
		return cusId;
	}
	public void setBstatus(int status)
	{
		this.status=status;
	}
	public int getBstatus()
	{
		return status;
	}
	
}

