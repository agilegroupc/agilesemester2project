package Suriya.DB;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class NewsAgentDAO {

	private static DBUtils dbc;
	private static Connection conn=null;
	private static PreparedStatement st=null;
	private static ResultSet rs=null;
	private Statement stmt = null;

	public NewsAgentDAO() {
		try {
			dbc=DBUtils.getDBConnection();
			conn = dbc.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void setConnection(String driver,String url,String username,String password) {
		try {
			dbc.setConnection(driver, url, username, password);
			conn=dbc.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
	}
	
	public int registerInsert(TBNewsAgent newsagent) {
		int num=-2;
		try {
			String sql = "insert into newsagent(managerName,phone,email,pass) values(?,?,?,?)";
			st = conn.prepareStatement(sql);
			// index starts from 1
			st.setString(1, newsagent.getManagerName());
			st.setString(2, newsagent.getPhone());
			st.setString(3, newsagent.getEmail());
			st.setString(4, newsagent.getPassword());
			// execute and return number of rows that take effect
			num = st.executeUpdate();
			return num;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return num;
	}

	public String queryPassword(String email) {
		
		String status=null;
		String sql = "select pass from newsagent where email=?";
		try {
			st = conn.prepareStatement(sql);
			st.setString(1, email);
			rs=st.executeQuery();
			while(rs.next()) {
				status=rs.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return status;
	}
	
	public int updateCustomer(TBCustomer newCus, int id)
	{
		int num=-2;
		int Id = id;
		String status=null;
		String sql = "update customers SET firstName=?,lastName=?,age=?,gender=?,phone=?,email=?,address=? WHERE customer_id=?";
		try {
			st = conn.prepareStatement(sql);
			st.setString(1, newCus.getFirstName());
			st.setString(2, newCus.getLastName());
			st.setInt(3, newCus.getAge());
			st.setString(4, newCus.getGender());
			st.setString(5, newCus.getPhone());
			st.setString(6, newCus.getEmail());
			st.setString(7, newCus.getAddress());
			st.setInt(8, Id);
			num = st.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return num;
		
	}
	public int updateArea(TBNewsAgent newAgent, int id)
	{
		int num=-2;
		int ID= id;
		String status = null;
		String sql="update deliveryperson SET area=? where dpId=?";
		try {
			st= conn.prepareStatement(sql);
			st.setInt(1, newAgent.getArea());
			st.setInt(2, newAgent.getDpID());
			num =st.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return num;
		
	}
	public int updateAreaTest(int area, int id)
	{
		int num=-2;
		int ID= id;
		int Area =area;
		String status = null;
		String sql="update deliveryperson SET area=? where dpId=?";
		try {
			st= conn.prepareStatement(sql);
			st.setInt(1, Area);
			st.setInt(2, ID);
			num =st.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return num;
		
	}
	
	public int updateBillingStatus(TBNewsAgent newAgent, int bStatus)
	{
		int num=-2;
		int sta = bStatus;
		String status=null;
		String sql = "update customers SET billing_status=? where customer_id=?";
		try {
			st = conn.prepareStatement(sql);
			st.setInt(1, newAgent.getBstatus());
			st.setInt(2, newAgent.getcusId());
			num=st.executeUpdate();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return num;
		
	}
	public int updateBillingStatusTest(int id, int bStatus)
	{
		int num=-2;
		int ID = id;
		int sta = bStatus;
		String status=null;
		String sql = "update customers SET billing_status=? where customer_id=?";
		try {
			st = conn.prepareStatement(sql);
			st.setInt(1, sta);
			st.setInt(2, ID);
			num=st.executeUpdate();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return num;
		
	}
	
	public int printDeliveryNote(TBNewsAgent newAgent, int dId)
	{
		int num=-2;
		int id= dId;
		String status = null;
		int area;
		String method = null;
		String name = null;
		String cName = null;
		String address = null;
		int custId = 0;
		String sql = "select area,fName from deliveryperson where dpId=?";
		String sql1 = "select order_id from deliverydocket where area=?";
		String sql2 = "select payment_method,customer_id from orders where order_id=?";
		String sql3 = "select firstName,lastName,address from customers where customer_id=?";
		try {
			st=conn.prepareStatement(sql);
			st.setInt(1, id);
			rs=st.executeQuery();
			while(rs.next())
			{
				status = rs.getString(1);
				name = rs.getString(2);
			}
			area = Integer.parseInt(status);
			st=conn.prepareStatement(sql1);
			st.setInt(1,area);
			rs=st.executeQuery();
			while(rs.next())
			{
				status = rs.getString(1);
			}
			int orderId = Integer.parseInt(status);
			st = conn.prepareStatement(sql2);
			st.setInt(1, orderId);
			rs=st.executeQuery();
			while(rs.next())
			{
				method = rs.getString(1);
				custId = rs.getInt(2);
			}
			st = conn.prepareStatement(sql3);
			st.setInt(1, custId);
			rs=st.executeQuery();
			while(rs.next())
			{
				cName = rs.getString(1);
				cName = cName + rs.getString(2);
				address = rs.getString(3);
			}
			BufferedWriter writer;
			try {
				writer = new BufferedWriter(new FileWriter(new File("Delivery Note_"+name+".txt")));
				writer.write("Name: "+ name);
				writer.newLine();
				writer.write(" Method: "+method);
				writer.newLine();
				writer.write(" Area: "+area);
				writer.newLine();
				writer.write(" Order id: "+orderId);
				writer.newLine();
				writer.write(" Customer Name: "+cName);
				writer.newLine();
				writer.write(" Address: "+address);
				writer.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return num;
		
	}
}


