package StartupGUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;

import AbhikRai.GUI.LoginDP;
import Suriya.newsagent.newsAgentStartUp;
import WangPengXin.GUI.CustomerLogin;

import java.awt.Font;
import javax.swing.SwingConstants;

public class TempSartup extends JFrame {

	private JFrame frmAthloneDeliverySystem;
	JButton btnCustomer = new JButton("Customer");
	JButton btnDeliveryPerson = new JButton("Delivery Person");
	private final JButton btnNewsagent = new JButton("Newsagent");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TempSartup window = new TempSartup();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		});
	}

	/**
	 * Create the application.
	 */
	public TempSartup() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAthloneDeliverySystem = new JFrame();
		frmAthloneDeliverySystem.setTitle("Athlone Delivery System");
		frmAthloneDeliverySystem.setBounds(100, 100, 571, 391);
		frmAthloneDeliverySystem.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAthloneDeliverySystem.getContentPane().setLayout(null);

		btnCustomer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					CustomerLogin window = new CustomerLogin();
				} catch (Exception e) {
					e.printStackTrace();
				}
				frmAthloneDeliverySystem.dispose();
			}
		});
		btnCustomer.setBounds(166, 109, 170, 36);
		frmAthloneDeliverySystem.getContentPane().add(btnCustomer);

		btnDeliveryPerson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmAthloneDeliverySystem.dispose();
				LoginDP window = new LoginDP();
			}
		});
		btnDeliveryPerson.setBounds(166, 255, 170, 36);
		frmAthloneDeliverySystem.getContentPane().add(btnDeliveryPerson);

		JLabel lblWelcome = new JLabel("WELCOME TO ATHLONE DELIVERY SYSTEM");
		lblWelcome.setHorizontalAlignment(SwingConstants.CENTER);
		lblWelcome.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblWelcome.setBounds(14, 34, 525, 36);
		frmAthloneDeliverySystem.getContentPane().add(lblWelcome);
		btnNewsagent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new newsAgentStartUp();
				frmAthloneDeliverySystem.dispose();
			}
		});
		btnNewsagent.setBounds(166, 180, 170, 41);

		frmAthloneDeliverySystem.getContentPane().add(btnNewsagent);

		// center window
		frmAthloneDeliverySystem.setLocationRelativeTo(null);

		frmAthloneDeliverySystem.setVisible(true);
	}
}
