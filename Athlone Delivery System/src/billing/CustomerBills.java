package billing;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import Suriya.DB.NewsAgentDAO;
import Suriya.DB.QueryTableModel;
import Suriya.DB.TBNewsAgent;
import Suriya.newsagent.DialogMessage;
import Suriya.newsagent.NewsAgentMainPage;
import Suriya.newsagent.NewsagentBillingPage;

public class CustomerBills extends JFrame{

	private JFrame Billing;
	private JTextField txtId;
	TBNewsAgent newAgent = new TBNewsAgent();
	NewsAgentDAO newDao = new NewsAgentDAO();
	QueryTableModel TableModel = new QueryTableModel();
	private JTable table = new JTable(TableModel);	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CustomerBills window = new CustomerBills();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CustomerBills() {
		initialize();
		Billing.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		Billing = new JFrame();
		Billing.setTitle("Customer Bills");
		Billing.setBounds(100, 100, 1126, 601);
		Billing.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Billing.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Enter Customer ID:");
		lblNewLabel.setBounds(46, 30, 150, 14);
		Billing.getContentPane().add(lblNewLabel);
		
		txtId = new JTextField();
		txtId.setBounds(278, 27, 124, 20);
		Billing.getContentPane().add(txtId);
		txtId.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Enter Status:");
		lblNewLabel_1.setBounds(46, 69, 150, 14);
		Billing.getContentPane().add(lblNewLabel_1);
		
		JList list = new JList();
		list.setBounds(161, 82, 110, -16);
		Billing.getContentPane().add(list);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(1, 1, 2, 1));
		spinner.setBounds(278, 66, 124, 20);
		Billing.getContentPane().add(spinner);
		
		JButton btnStatus = new JButton("Change Status");
		btnStatus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int num=0;
				
				int sta = Integer.parseInt(spinner.getValue().toString());
				if(txtId.getText().toString().trim().equals(""))
				{
					Suriya.newsagent.DialogMessage.showInfoDialog("Customer ID is empty");
					return;
				}
				int cusId;
				try {
					cusId = Integer.parseInt(txtId.getText());
				     System.out.println("An integer");
				}
				catch (NumberFormatException ex) {
				     //Not an integer
					DialogMessage.showWarningDialog("Characters are not allowed!");
					return;
				}
				newAgent.setcusId(cusId);
				newAgent.setBstatus(sta);
				num = newDao.updateBillingStatus(newAgent,sta);
				if(num>0)
				{
					DialogMessage.showInfoDialog("Successful");
					TableModel.callCustomer(1);
					table.setModel(TableModel);
				}
			}
		});
		btnStatus.setBounds(46, 414, 356, 23);
		Billing.getContentPane().add(btnStatus);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(46, 101, 1032, 279);
		Billing.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JButton btnNewButton = new JButton("Back");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new NewsAgentMainPage();
				Billing.dispose();
			}
		});
		btnNewButton.setBounds(46, 357, 356, 23);
		Billing.getContentPane().add(btnNewButton);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Billing.dispose();
				new NewsagentBillingPage();
			}
		});
		btnBack.setBounds(722, 518, 356, 23);
		Billing.getContentPane().add(btnBack);
		
		Billing.setLocationRelativeTo(null);
	}
}
