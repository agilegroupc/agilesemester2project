package billing;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;

import AbhikRai.DB.DAO.DeliveryPersonDAO;
import AbhikRai.GUI.QueryModel;
import Suriya.newsagent.NewsagentBillingPage;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class BillsManagement {

	private static final String BILL_DETAIL="select bill_id, billing.customer_id, billing.order_id, "
			+ "customers.email, customers.address, item_name, cost as unit_price, order_type, total_amount, order_status, billing.billing_status, overdue " 
			+ "from orders,items,billing,customers "
			+ "where orders.item_id=items.item_id and orders.order_id=billing.order_id and customers.customer_id=billing.customer_id "
			+ "and order_status!=\"Canceled\" ";
	private DeliveryPersonDAO dpDao=new DeliveryPersonDAO();
	private JFrame frmBillsManagement;
	
	//Jtable
	private JScrollPane scrollPane;
	private QueryModel TableModel = new QueryModel();
	private JTable TableofDBContents = new JTable(TableModel);
	
	private JButton btnGenBills = new JButton("Generate Bills");
	private JButton btnGenOverdue = new JButton("Generate Overdue Bills");
	private JButton btnBack = new JButton("Back");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BillsManagement window = new BillsManagement();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public BillsManagement() {
		initialize();
		frmBillsManagement.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmBillsManagement = new JFrame();
		frmBillsManagement.setTitle("Bills Management");
		frmBillsManagement.setBounds(100, 100, 1120, 733);
		frmBillsManagement.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmBillsManagement.getContentPane().setLayout(null);
		
		scrollPane = new JScrollPane(TableofDBContents, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBackground(Color.LIGHT_GRAY);
		scrollPane.setBounds(24, 80, 1064, 393);
		frmBillsManagement.getContentPane().add(scrollPane);
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmBillsManagement.dispose();
				new NewsagentBillingPage();
			}
		});
		
		btnBack.setBounds(24, 616, 138, 44);
		frmBillsManagement.getContentPane().add(btnBack);
		btnGenBills.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TableModel.outputToTable(BILL_DETAIL);
			}
		});
		btnGenBills.setBounds(24, 490, 159, 36);
		
		frmBillsManagement.getContentPane().add(btnGenBills);
		btnGenOverdue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TableModel.outputToTable(BILL_DETAIL+"and overdue=1");
			}
		});
		btnGenOverdue.setBounds(872, 490, 216, 36);
		
		frmBillsManagement.getContentPane().add(btnGenOverdue);
		
		//center window
		frmBillsManagement.setLocationRelativeTo(null);
	}
	
}
