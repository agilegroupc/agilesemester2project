package billing;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import AbhikRai.DialogMessage;
import AbhikRai.VerifyBillingDetails;
import AbhikRai.DB.DAO.DeliveryPersonDAO;
import AbhikRai.DB.Table.TBbill;
import AbhikRai.GUI.QueryModel;
import Suriya.newsagent.NewsagentBillingPage;

public class TranferBillsGUI {

	private JFrame frmTransferBills;
	private JTextField txtbillid;
	private JTextField txtcustid;
	private JTextField txtorderid;
	private JTextField txtbillstatus;
	private JTextField txtcost;
	private JTextField txtoverdue;
	
	private DeliveryPersonDAO dpDao=new DeliveryPersonDAO();
	QueryModel TableModel = new QueryModel();
	private JTable table = new JTable(TableModel);
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TranferBillsGUI window = new TranferBillsGUI();
					window.frmTransferBills.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TranferBillsGUI() {
		initialize();
		frmTransferBills.setVisible(true);
		TableModel.call(1);
		table.setModel(TableModel);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmTransferBills = new JFrame();
		frmTransferBills.setTitle("Transfer Bills");
		frmTransferBills.setBounds(100, 100, 645, 587);
		frmTransferBills.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTransferBills.getContentPane().setLayout(null);
		
		JLabel lblInsert = new JLabel("TRANSFER BILLS");
		lblInsert.setBounds(224, 13, 199, 36);
		lblInsert.setFont(new Font("Calibri", Font.BOLD, 29));
		frmTransferBills.getContentPane().add(lblInsert);
		
		JLabel lblcustid = new JLabel("Customer Id: ");
		lblcustid.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblcustid.setBounds(135, 91, 186, 22);
		frmTransferBills.getContentPane().add(lblcustid);
		
		txtcustid = new JTextField();
		txtcustid.setBounds(348, 93, 189, 20);
		frmTransferBills.getContentPane().add(txtcustid);
		txtcustid.setColumns(10);
		
		JLabel lblorderid = new JLabel("Order Id: ");
		lblorderid.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblorderid.setBounds(135, 128, 117, 14);
		frmTransferBills.getContentPane().add(lblorderid);
		
		txtorderid = new JTextField();
		txtorderid.setBounds(348, 126, 186, 20);
		frmTransferBills.getContentPane().add(txtorderid);
		txtorderid.setColumns(10);
		
		JLabel lblamount = new JLabel("Total Amount: ");
		lblamount.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblamount.setBounds(135, 166, 141, 14);
		frmTransferBills.getContentPane().add(lblamount);
		
		txtcost = new JTextField();
		txtcost.setBounds(348, 164, 189, 20);
		frmTransferBills.getContentPane().add(txtcost);
		txtcost.setColumns(10);
		
		JLabel lblbillstatus = new JLabel("Bill Status: ");
		lblbillstatus.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblbillstatus.setBounds(135, 199, 100, 14);
		frmTransferBills.getContentPane().add(lblbillstatus);
		
		txtbillstatus = new JTextField();
		txtbillstatus.setBounds(348, 197, 189, 20);
		frmTransferBills.getContentPane().add(txtbillstatus);
		txtbillstatus.setColumns(10);
		
		JLabel lbloverdue = new JLabel("Overdue By: ");
		lbloverdue.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lbloverdue.setBounds(135, 232, 100, 14);
		frmTransferBills.getContentPane().add(lbloverdue);
		
		txtoverdue = new JTextField();
		txtoverdue.setBounds(348, 230, 189, 20);
		frmTransferBills.getContentPane().add(txtoverdue);
		txtoverdue.setColumns(10);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmTransferBills.dispose();
				new NewsagentBillingPage();
			}
		});
		btnBack.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnBack.setBounds(56, 501, 100, 28);
		frmTransferBills.getContentPane().add(btnBack);
		
		
		JButton btnRegister = new JButton("Insert");
		btnRegister.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(txtcustid.getText().trim().equals("") || txtorderid.getText().trim().equals("") ||
						txtcost.getText().trim().equals("") || txtbillstatus.getText().trim().equals("") || txtoverdue.getText().trim().equals("")) {
					DialogMessage.showWarningDialog("Empty bill info!");
					return;
				}
				
				int custid = Integer.parseInt(txtcustid.getText());
				int orderid=Integer.parseInt(txtorderid.getText());
				int amt=Integer.parseInt(txtcost.getText());
				int billstatus=Integer.parseInt(txtbillstatus.getText());
				int overdue=Integer.parseInt(txtoverdue.getText());
				
//				if(!VerifyBillingDetails.verifyBillingStatus(billstatus)) {
//					DialogMessage.showWarningDialog("Invalid status format!");
//					return;
//				}
//				if(!VerifyBillingDetails.verifyAmount(amt)) {
//					DialogMessage.showWarningDialog("Invalid amount!");
//					return;
//				}
				TBbill bill=new TBbill();
				bill.setCustomerId(custid);
				bill.setOrderId(orderid);
				bill.setBillstatus(billstatus);
				bill.setAmount(amt);
				bill.setOverdue(overdue);
				int num = dpDao.insertBill(bill);
				if (num > 0) {
					DialogMessage.showInfoDialog("Transfered successfully!!!");
					TableModel.call(1);
					table.setModel(TableModel);
				}
			}
		});
		btnRegister.setBounds(426, 504, 111, 23);
		frmTransferBills.getContentPane().add(btnRegister);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 275, 600, 200);
		frmTransferBills.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		frmTransferBills.setLocationRelativeTo(null);
	}
}
