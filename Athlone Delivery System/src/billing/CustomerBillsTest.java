package billing;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Suriya.DB.DBUtils;
import Suriya.DB.NewsAgentDAO;
import Suriya.DB.TBNewsAgent;

public class CustomerBillsTest {

	@Before
	public void setUp() throws Exception {
	}
	@Test
	public void testGetConnection() throws Exception{
		DBUtils dbc;
		
			dbc = DBUtils.getDBConnection();
			assertNotEquals(null, dbc.getConnection());
		
	}
	//Test for update
	@Test
	public void testBstatusUpdateF() {
		NewsAgentDAO newDao = new NewsAgentDAO();
		TBNewsAgent agent = new TBNewsAgent();
		agent.setArea(1);
		newDao.setConnection("com.mysql.cj.jdbc.Driver",
				"jdbc:mysql://localhost:3306/athlone_delivery_system?serverTimezone=UTC", 
				"root", "password");
		assertEquals(0, newDao.updateBillingStatus(agent, 1));
	}
	@Test
	public void testBstatusUpdateP() {
		NewsAgentDAO newDao = new NewsAgentDAO();
		newDao.setConnection("com.mysql.cj.jdbc.Driver",
				"jdbc:mysql://localhost:3306/athlone_delivery_system?serverTimezone=UTC", 
				"root", "password");
		assertEquals(1, newDao.updateBillingStatusTest(1, 1));
	}
	
}
